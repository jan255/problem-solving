/**
* https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/breakup-app/
*/ 
#include <bits/stdc++.h>
using namespace std;


vector<string> split_str(string& s) {
  vector<string> v;
  while (s.find(' ') != string::npos) {
    int i = s.find(' ');
    v.push_back(s.substr(0, i));
    s = s.substr(i + 1);
  }
  v.push_back(s);
  return v;
}

int main() {
  int n; cin >> n;
  string s;
  getline(cin, s);
  int days[40];
  for (int i = 0; i < 40; ++i)
    days[i] = 0;
  while (n-- > -1) {
    getline(cin, s);
    char fc = s[0];
    vector<string> v = split_str(s);
    for (int i = 0; i < v.size(); ++i) {
      string& si = v[i];
      if (si[0] >= '0' && si[0] <= '9') {
        int mon = (si[0] - '0');
        if (si.length() > 1) mon = mon * 10 + si[1] - 48;
        days[mon] += fc == 'G' ? 2 : 1;
      }
    }
  }
  vector<int> v;
  for (int i = 1; i <= 31; ++i) {
    if (v.size() == 0 || days[v[0]] == days[i]) {
      v.push_back(i);
    } else if (days[v[0]] < days[i]) {
      v.clear();
      v.push_back(i);
    }
  }

  if (v.size() == 0 || v.size() > 1) {
    cout << "No Date";
  } else if (v[0] == 19 || v[0] == 20) {
    cout << "Date";
  } else {
    cout << "No Date";
  }


  return 0;
}