// https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/repeated-k-times/
#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int cnt[N];

int main() {
  int n; cin >> n;
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    cnt[x]++;
  }
  int k; cin >> k;
  for (int i = 0; i < N; ++i)
    if (cnt[i] == k) {
      cout << i << endl;
      break;
    }

  return 0;
}