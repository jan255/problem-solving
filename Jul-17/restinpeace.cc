// https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/rest-in-peace-21-1/
#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    string s = "The streak lives still in our heart!";
    if (n % 21 == 0) s = "The streak is broken!";
    while (n) {
      if ((n % 100) == 21) s = "The streak is broken!";
      n /= 10;
    }
    cout << s << endl;
  }  	

  return 0;
}