/**
* https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/square-transaction-20/ 
*/

#include <bits/stdc++.h>
using namespace std;

const int T = 1e5 + 1;
int ps[T];

int main() {
  int t; cin >> t;
  for (int i = 0; i < t; ++i) {
    cin >> ps[i];
    if (i) ps[i] += ps[i - 1];
  }

  int q; cin >> q;
  while (q--) {
    int tar; cin >> tar;
    int l = 0, r = t - 1;
    while (l < r) {
      int mid = (l + r) >> 1;
      if (ps[mid] >= tar) r = mid;
      else l = mid + 1;
    }
    cout << (ps[t - 1] >= tar ? l + 1 : -1) << endl;
  }

  return 0;
}