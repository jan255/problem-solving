// http://codeforces.com/contest/825/problem/C

#include <bits/stdc++.h>
using namespace std;

int main() {
  int n, k;
  cin >> n >> k;
  vector<int> v;
  for (int i = 0; i < n; ++i) {
    int ai; cin >> ai;
    v.push_back(ai);
  }

  sort(v.begin(), v.end());

  int ans = 0;
  for (int i = 0; i < n; ++i) {
    int ai = max(k, i ? v[i - 1] : 0);
    int d = v[i] / 2 + (v[i] & 1);
    while (ai < d) {
      ai <<= 1;
      ans++;
    }
  }
  cout << ans << endl;

  return 0;
}