/**
* https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/min-max-8/
*/
#include <bits/stdc++.h>
using namespace std;


int main() {
  vector<long long> v;
  int N; cin >> N;
  long long sum = 0;
  for (int i = 0; i < N; ++i) {
    long long ai; cin >> ai;
    v.push_back(ai);
    sum += ai;
  }
  long long mini = 1e18 + 18;
  long long maxi = 0;
  for (int i = 0; i < N; ++i) {
    mini = min(mini, sum - v[i]);
    maxi = max(maxi, sum - v[i]);
  }
  cout << mini << ' ' << maxi << endl;

  return 0;
}