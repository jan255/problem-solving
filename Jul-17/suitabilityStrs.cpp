// http://codeforces.com/contest/825/problem/D
#include <bits/stdc++.h>
using namespace std;

const int N = 26;
int c[N];
string s, t;
int tc[N];

int main() {
  cin >> s;
  cin >> t;

  vector<int> qs;
  for (int i = 0; i < s.length(); ++i) {
    if (s[i] == '?') qs.push_back(i);
    else ++c[s[i] - 97];
  }

  for (int i = 0; i < t.length(); ++i)
    ++tc[t[i] - 97];

  int qsi = 0;
  while (qsi < qs.size()) {
    int num = 1e7;
    for (int i = 0; i < 26; ++i) {
      if (tc[i]) {
        while (qsi < qs.size() &&
                c[i] / tc[i] == 0) {
          ++c[i];
          s[qs[qsi]] = i + 97;
          ++qsi;
        }
        num = min(num, c[i] / tc[i]);
      }
    }
    for (int i = 0; i < 26; ++i) {
      if (tc[i]) {
        c[i] -= num * tc[i];
      }
    }
  }
  cout << s << endl;

  return 0;
}