// https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/holiday-season-ab957deb/
#include <bits/stdc++.h>
using namespace std;

const int N = 2345;
vector<vector<int> > ps(N, vector<int>(26));

int main() {
  int n; cin >> n;
  string s; cin >> s;
  for (int i = 0; i < s.length(); ++i) {
    ps[i][s[i] - 97]++;
    if (i) {
      for (int j = 0; j < 26; ++j)
        ps[i][j] += ps[i - 1][j];
    }
  }

  long long is = 0;
  for (int i = 1; i < n - 2; ++i) {
    for (int j = i + 2; j < n; ++j) {
      if (s[i] == s[j]) {
        for (int k = 0; k < 26; ++k) {
          is += ps[i - 1][k] *
                  (ps[j - 1][k] - ps[i][k]);
        }
      }
    }
  }
  cout << is << endl;

  return 0;
}