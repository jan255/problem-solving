// http://codeforces.com/contest/825/problem/B
#include <bits/stdc++.h>
using namespace std;

bool in(int x, int y) {
  return x >= 0 && y >= 0 && x < 10 && y < 10;
}

int main() {
  string s[10];
  for (int i = 0; i < 10; ++i)
    cin >> s[i];

  bool yes = false;
  int dx[4] = {1, 1, 0, 1};
  int dy[4] = {1, -1, 1, 0};
  for (int i = 0; i < 10; ++i) {
    for (int j = 0; j < 10; ++j) {
      for (int k = 0; k < 4; ++k) {
        if (in(i + dx[k] * 4, j + dy[k] * 4)) {
          int x = 0;
          for (int l = 0; l < 5; ++l) {
            int ii = i + dx[k] * l;
            int jj = j + dy[k] * l;
            if (s[ii][jj] == 'X') x++;
            if (s[ii][jj] == 'O') x--;
          }
          yes = yes or (x == 4);
        }
      }
    }
  }
  cout << (yes ? "YES" : "NO") << endl;

  return 0;
}


// int main() {
//   string s[10];
//   for (int i = 0; i < 10; ++i)
//     cin >> s[i];

//   bool yes = false;
//   for (int i = 0; i < 10; ++i) {
//     for (int j = 0; j < 10; ++j) {
//       int x = 0;
//       for (int k = 0; k < 5; ++k) {
//         int ii = i + k;
//         if (ii < 10) {
//           x += s[ii][j] == 'X';
//           x -= s[ii][j] == 'O';
//         }
//       }
//       yes = yes or (x == 4 && i < 6);
//       x = 0;
//       for (int k = 0; k < 5; ++k) {
//         int jj = j + k;
//         if (jj < 10) {
//           x += s[i][jj] == 'X';
//           x -= s[i][jj] == 'O';
//         }
//       }
//       yes = yes or (x == 4 && j < 6);
//       x = 0;
//       for (int k = 0; k < 5; ++k) {
//         int ii = i + k;
//         int jj = j + k;
//         if (ii < 10 && jj < 10) {
//           x += s[ii][jj] == 'X';
//           x -= s[ii][jj] == 'O';
//         }
//       }
//       yes = yes or (x == 4 && i < 6 && j < 6);
//       x = 0;
//       for (int k = 0; k < 5; ++k) {
//         int ii = i + k;
//         int jj = j - k;
//         if (jj >= 0 && ii < 10 && jj < 10) {
//           x += s[ii][jj] == 'X';
//           x -= s[ii][jj] == 'O';
//         }
//       }
//       yes = yes or (x == 4 && i < 6 && j >= 4);
//     }
//   }    
//   cout << (yes ? "YES" : "NO") << endl;  

//   return 0;
// }