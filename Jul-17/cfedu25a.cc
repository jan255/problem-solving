// http://codeforces.com/contest/825/problem/A
#include <bits/stdc++.h>
using namespace std;


int main() {
  int n; cin >> n;
  string s; cin >> s;
  for (int i = 0; i < s.length(); ++i) {
    if (s[i] == '0' && i > 0 && s[i - 1] == '0')
      cout << 0;
    if (s[i] == '1') {
      int j = i;
      while (j < s.length() && s[j] == '1') ++j;
      cout << (j - i);
      i = j - 1;     
    }
  }
  if (s[s.length() - 1] == '0') cout << 0;

  return 0;
}