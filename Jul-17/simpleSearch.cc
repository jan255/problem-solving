/**
* https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/simple-search-4/
*/

#include <bits/stdc++.h>
using namespace std;

int main() {
  vector<int> v;
  int n; cin >> n;
  while (n--) {
    int x; cin >> x;
    v.push_back(x);
  }	
  int k; cin >> k;
  for (int i = 0; i < v.size(); ++i) {
    if (v[i] == k) {
      cout << i;
      break;
    }
  }

	return 0;
}