/**
* http://codeforces.com/contest/827/problem/A
* Curtosy: irajdeep
*/

#include <bits/stdc++.h>
using namespace std;

const int LEN = 2e6;
char s[3 + (LEN << 1)];

vector<string> ts;
vector<int> xs[LEN];

int main() {
  ios::sync_with_stdio(0);
  cin.tie(0);

  for (int i = 0; i < (LEN << 1); ++i)
    s[i] = 48;

  int l = 0;
  int n; cin >> n;
  for (int i = 0; i < n; ++i) {
    string t; cin >> t;
    ts.push_back(t);
    int k; cin >> k;
    while (k--) {
      int x; cin >> x;
      xs[x].push_back(i);
      l = max(l, (int)t.length() + x - 1);
    }
  }

  for (int i = 1; i <= l; ++i) {
    vector<int>& v = xs[i];
    for (int j = 0; j < v.size(); ++j) {
      string& t = ts[v[j]];
      int k = i + (int)t.length() - 1;
      while (k >= i && s[k] == 48) {
        s[k] = t[k - i];
        k--;
      }
    }
    if (v.empty() && s[i] == 48) {
      s[i] = 97;
    }
  }

  for (int i = 1; i <= l; ++i)
    cout << s[i];
  cout << endl;

  return 0;
}