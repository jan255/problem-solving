/**
* http://codeforces.com/contest/264/problem/B
* courtsy: irajdeep
*/

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Queue;
import java.util.Scanner;
import java.util.TreeMap;

public class GoodSeqs {
	
	private static Scanner input = new Scanner(System.in); 
	
	private static final int N = (int) 1e5 + 1;
	private static int DP[] = new int[N];
	private static Map<Integer, Integer> mp = new TreeMap<>();

	public static void main(String[] args) throws IOException {
		for (int i = 2; i < N; ++i) {
			if (!mp.containsKey(i)) mp.put(i, i);
			else continue;
			int j = i + i;
			while (j < N) {
				if (!mp.containsKey(j)) mp.put(j, i);
				j += i;
			}
		}

		List<Integer> arr = new ArrayList<>();
		int n = input.nextInt();
		for (int i = 0; i < n; ++i) {
			arr.add(input.nextInt());
		}
		
		Queue<Integer> pfs = new LinkedList<>();
		int i = arr.get(0) == 1 ? 1 : 0;
		for ( ; i < n; ++i) {
			int ai = arr.get(i);
			int subSol = 0;
			while (ai > 1) {
				int p = mp.get(ai);
				pfs.add(p);
				subSol = Math.max(subSol, DP[p]);
				while (ai % p == 0) ai /= p;
			}
			subSol++;
			while (!pfs.isEmpty()) {
				int p = pfs.poll();
				DP[p] = subSol;
			}
		}
		if (n == 1 && arr.get(0) == 1)
			DP[1] = 1;
		Arrays.sort(DP);
		System.out.println(DP[N - 1]);
		
		GoodSeqs.tearDown();
	}
	
	private static void tearDown() {
		input.close();
	}
}
