/**
* https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/mannas-first-name-4/
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int find_match(char* s, int i, char* pat) {
	int j = 0;
	while (j < strlen(pat) && i + j < strlen(s)) {
		if (pat[j] != s[i + j]) break;
		j++;
	}
	return j == strlen(pat);
}

int main() {
	int T; scanf("%d", &T);
	while (T-- > 0) {
		char s[156];
		scanf("%s", s);
		int i = 0;
		int suvo = 0;
		int suvojit = 0;
		while (i < strlen(s)) {
			if (find_match(s, i, "SUVOJIT")) {
				suvojit++;
			} else if (find_match(s, i, "SUVO")) {
				suvo++;
			}
			i++;
		}
		printf("SUVO = %d, SUVOJIT = %d\n", suvo, suvojit);
	}

	return 0;
}