#include <bits/stdc++.h>
using namespace std;


int main() {
  int n; cin >> n;
  string s; getline(cin, s);
  cout << s << endl;
  int c = 0; int vol = 0;
  for (int i = 0; i < n; ++i) {
    c += s[i] >= 65 && s[i] <= 90; 
    if (s[i] == ' ') {
      vol = max(vol, c);
      c = 0;
    }
  }
  cout << vol << endl;

  return 0;
}