#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int n, m;
vector<int> g[N];
set<int> h[N];
bool vis[N];
int perm[N];
vector<int> topo;

void dfs(int v) {
  if (vis[v]) return ;
  vis[v] = true;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    dfs(u);
  }
  topo.push_back(v);
}

bool comp(const int& a, const int& b) {
  if (a > b && h[a].find(b) == h[a].end()) {
    return false;
  }
  return true;
}

int main() {
  cin >> n >> m;
  while (m--) {
    int u, v;
    cin >> u >> v;
    g[u].push_back(v);
    h[u].insert(v);
  }

  for (int i = 1; i <= n; ++i) {
    dfs(i);
  }

  for (int i = 0; i < n / 2; ++i) {
    swap(topo[i], topo[n - i - 1]);
  }
  sort(topo.begin(), topo.end(), comp);
  for (int i = 1; i <= n; ++i) {
    cout << topo[i - 1] << " ";
    perm[topo[i - 1]] = i;  
  }
  cout << endl;

  for (int i = 1; i <= n; ++i)
    cout << perm[i] << " ";
  cout << endl;


  return 0;
}