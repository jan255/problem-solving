// https://www.hackerearth.com/practice/algorithms/searching/linear-search/practice-problems/algorithm/the-normal-type/
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

public class TheNormalType {

	private static Scanner input = new Scanner(System.in);

	public static void main(String[] args) {
		int N = input.nextInt();
		
		Map<Integer, Integer> mp = new HashMap<>();
		List<Integer> ar = new ArrayList<>();
		for (int i = 0; i < N; ++i) {
			int ai = input.nextInt();
			ar.add(ai);
			if (!mp.containsKey(ai)) mp.put(ai, 0);
			mp.put(ai, mp.get(ai) + 1);
		}
		input.close();
		
		long subars = 0;
		
		Map<Integer, Integer> m = new HashMap<>();
		int l = 0;
		int r = 0;
		while (l < N) {
			while (r < N && mp.size() > m.size()) {
				int ai = ar.get(r);
				if (!m.containsKey(ai)) m.put(ai, 0);
				m.put(ai, m.get(ai) + 1);
				r++;
			}
			
			if (mp.size() == m.size()) {
				subars += N - r + 1;
			}

			int li = ar.get(l);
			m.put(li, m.get(li) - 1);
			if (m.get(li) == 0) m.remove(li);

			l++;
		}
		
		System.out.println(subars);
	}
	
}
