import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
import java.util.TreeMap;

public class LruCache {
	
	static class Solver {
		
		private TreeMap<Integer, Long> keyToTime = new TreeMap<>();
		private TreeMap<Long, Integer> timeToKey = new TreeMap<>();
		
		private TreeMap<Integer, Integer> keyToValue = new TreeMap<>();
		private Integer capacity = 1;
		
		public void solve() throws IOException {
			Scanner input = new Scanner(System.in);
			int t = input.nextInt();
			while (t-- > 0) {
				this.capacity = input.nextInt();
				int n = input.nextInt();
				keyToTime.clear();
				timeToKey.clear();
				keyToValue.clear();
				
				while (n-- > 0) {
					String op = input.next();
					if (op.equals("SET")) {
						int k = input.nextInt();
						int val = input.nextInt();
						this.set(k, val);
					} else if (op.equals("GET")) {
						int k = input.nextInt();
						System.out.print(this.get(k) + " ");
					}
				}
				System.out.println();
			}
			
			input.close();
		}

		private int get(int k) {
			if (this.keyToValue.containsKey(k)) {
				int val = keyToValue.get(k);
				Long lastTime = keyToTime.get(k);
				keyToValue.remove(k);
				keyToTime.remove(k);
				timeToKey.remove(lastTime);
				this.set(k, val);
				return val;
			}
			return -1;
		}

		private void set(int k, int val) {
			if (this.keyToValue.containsKey(k)) return;
			
			invalidateCache();
			
			Long currTime = new Date().getTime() + this.keyToValue.size();
			keyToValue.put(k, val);
			timeToKey.put(currTime, k);
			keyToTime.put(k, currTime);
		}

		private void invalidateCache() {
			if (this.keyToValue.size() < this.capacity) return;
			
			Long oldest = timeToKey.firstKey();
			Integer oldestKey = timeToKey.get(oldest);
			keyToValue.remove(oldestKey);
			timeToKey.remove(oldest);
			keyToTime.remove(oldestKey);
		}
	}
	
	public static void Main(String[] args) throws IOException {
		new Solver().solve();
	}
}
