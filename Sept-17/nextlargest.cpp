#include <bits/stdc++.h>
using namespace std;

const int N = 1e6;
int a[N];

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    for (int i = 0; i < n; ++i)
      cin >> a[i];
    set<int> st;
    for (int i = n - 1; i >= 0; --i) {
      set<int>::iterator it = st.upper_bound(a[i]);
      st.insert(a[i]);
      if (it == st.end()) a[i] = -1;
      else a[i] = *it;  
    }

    for (int i = 0; i < n; ++i)
      cout << a[i] << " ";
    cout << endl;
  } 

  return 0;
}