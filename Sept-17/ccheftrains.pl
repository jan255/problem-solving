#!/usr/bin/perl

use strict;
use warnings;

sub getNext {
	chomp(my $line = <>);
	return $line;
}

my $t = getNext();
foreach (1..$t) {
	my $pairs = getNext();
	my %stns = ();
	while ($pairs--) {
		my $key = getNext();
		$stns{$key} = getNext();
	}
	my %valuesHash = ();
	$valuesHash{$_} = 0 foreach values %stns;
	my $st = 0;
	foreach (keys %stns) {
		my $key = $_;
		$st = $key if !exists $valuesHash{$key};
	}
	while (exists $stns{$st}) {
		printf "%s-%s ", $st, $stns{$st};
		$st = $stns{$st};
	}
	printf "\n";
}