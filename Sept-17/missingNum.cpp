#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t-- > 0) {
    int n; cin >> n;
    int sum = 0;
    for (int i = 1; i < n; ++i) {
      int x; cin >> x;
      sum += x;
    }
    cout << ( (n * (n + 1) / 2) - sum) << endl;
  } 

  return 0;
}