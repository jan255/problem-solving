#include <bits/stdc++.h>
using namespace std;


int main() {
  int T; cin >> T;
  while (T--) {
    int n, m, x;
    cin >> n >> m >> x;
    vector<int> a(n);
    for (int i = 0; i < n; ++i)
      cin >> a[i];
    vector<int> b(m);
    vector<int> idx(m);
    for (int i = 0; i < m; ++i) {
      cin >> b[i];
      idx[i] = i;
    }

    sort(idx.begin(), idx.end(), [b](const int& i, const int& j) {
      return b[i] <= b[j];
    });

    vector<pair<int, int> > pairs;
    for (int i = 0; i < n; ++i) {
      int l = 0, r = n - 1;
      while (l < r) {
        int mid = (l + r) / 2;
        if (b[idx[mid]] >= a[i]) r = mid;
        else l = mid + 1;
      }
      while (l < n && b[idx[l]] == a[i])
        pairs.push_back(make_pair(i + 1, idx[l++] + 1));
    }

    if (pairs.empty()) {
      cout << -1 << endl;
    } else {
      cout << pairs[0].first << " " << pairs[0].second;
      for (int i = 1; i < pairs.size(); ++i)
        cout << << ", " << pairs[i].first << " " << pairs[i].second;
    }
  } 

  return 0;
}