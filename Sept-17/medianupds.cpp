#include <bits/stdc++.h>
using namespace std;

multiset<int> ms;
multiset<int>::iterator mid = ms.end();

void add(int x) {
  ms.insert(x);
  if (mid == ms.end()) mid = ms.begin();
  else if (ms.size() % 2) mid++;
}

void rm(int x) {
  multiset<int>::iterator it = ms.find(x);
  if (it == ms.end()) return;

  if (ms.size() == 1) {
    mid = ms.end();
    ms.erase(it);
  }
  else if (ms.size() % 2 == 0) {
    
  }
}

void med() {
  if (ms.size() == 0) {
    cout << "Wrong!" << endl;
    return;
  }
  if (ms.size() % 2) {
    cout << *mid << endl;
  } else {
    cout << (*mid + *(mid + 1)) / 2 << endl;
  }
}

int main() {
  int n; cin >> n;
  while (n--) {
    char c; cin >> c;
    int x; cin >> x;
    if (c == 'a') {
      add(x);
    } else if (c == 'r') {
      rm(x);
    }
    med();
  } 
 
  return 0;
}