#include <bits/stdc++.h>
using namespace std;

const int N = 3001;
int ds[N];

int root(int a) {
  return ds[a] < 0 ? a : (ds[a] = root(ds[a]));
}

bool same(int a, int b) {
  return root(a) == root(b);
}

void un(int a, int b) {
  if (same(a, b)) return;
  a = root(a);
  b = root(b);
  if (ds[a] > ds[b]) swap(a, b);
  ds[a] += ds[b];
  ds[b] = a;
}

struct Edge {
  int u;
  int v;
  int w;
};

bool comp(const struct Edge& a, const struct Edge& b) {
  if (a.w == b.w) return a.u + a.v + a.w < b.u + b.v + b.w;
  return a.w < b.w;
}

int main() {
  int n; cin >> n;
  int m; cin >> m;

  vector<Edge> v;
  for (int i = 0; i < m; ++i) {
    Edge e;
    cin >> e.u >> e.v >> e.w;
    v.push_back(e);
  }

  sort(v.begin(), v.end(), comp);
  
  for (int i = 1; i <= n; ++i)
    ds[i] = -1;

  un(v[0].u, v[0].v);
  long long tot = v[0].w;
  for (int i = 1; i < m; ++i) {
    int a = v[i].v;
    int b = v[i].u;
    int w = v[i].w;
    if (!same(a, b)) {
      un(a, b);
      tot += w;
    }
  }
  cout << tot << endl;

  return 0;
}