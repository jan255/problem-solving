#include <bits/stdc++.h>
using namespace std;

const int N = 1e5;
int Z[N];

void buildZ(string& s) {
  Z[0] = 0;
  int L = -1, R = -1;
  for (int i = 1; i < s.length(); ++i) {
    Z[i] = 0;
    if (L == -1 || i > R) {
      int l, r;
      for (l = 0, r = i; r < s.length() && s[l] == s[r]; l++, r++) ;
      Z[i] = l;
      if (l > 0) L = i, R = i + l - 1;
    }
    else {
      int k = i - L;
      if (Z[k] >= R - i + 1) {
        while (s[R - L + 1] == s[R + 1]) R++;
      }
      Z[i] = min(Z[k], R - i + 1);
    }

    cout << Z[i] << endl;
  }
}



int main() {
  string s = "fh$aliuhfldknvkjhiuxnjvdhgiudjkxnvjdfhkjhdkalsdjfhawliufh";
  buildZ(s);

  return 0;
}