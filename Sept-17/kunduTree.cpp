/*
  @author jan25 on 15/09/2017 at 0842
*/
#include <bits/stdc++.h>
using namespace std;

const long long MOD = 1E9 + 7;
const int N = 1e5 + 1;
map<int, int> cs;
int sizes[N];

int fillSizes(int v = 1, int p = -1) {
  sz[v] = 1;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u != p) {
      sz[v] += fillSizes(u, v);
    }
  }
  return sz[v];
}

long long compute(int v = 1, int p = -1) {
  long long res = 0;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u != p) {
      if (cs.[u] == v)
        res += (sizes[u] * 1LL * (sizes[1] - sizes[u] - 1)) % MOD;
      res = (res + compute(u, v)) % MOD;
    }
  }
  return res;
}

int main() {
  int n; cin >> n;
  for (int i = 1; i < n; ++i) {
    int u, v, c;
    cin >> u >> v >> c;
    if (c == 'r') 
      cs.insert(make_pair(u, v)), cs.insert(make_pair(v, u));


  }

  return 0;
}