
#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t--) {
    int n, m;
    cin >> n >> m;
    int l = 1, r = 1e8;
    while (l < r) {
      int mid = (l + r + 1) >> 1;
      if (mid * (mid + 1) / 2 > m) r = mid - 1;
      else l = mid;
    }
    cout << min(l, n) << endl;
  } 

  return 0;
}