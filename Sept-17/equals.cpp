#include <bits/stdc++.h>
using namespace std;

const int N = 1e4;
long long h[N];

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    for (int i = 0; i < n; ++i)
      cin >> h[i];
    sort(h, h + n);

    long long ar[3] = {0};
    for (int i = 1; i < n; ++i) {
      h[i] += ar[0] + 2 * ar[1] + 5 * ar[2];
      long long diff = h[i] - h[i - 1];
      ar[2] += diff  / 5;
      diff %= 5;
      ar[1] += diff / 2;
      diff %= 2;
      ar[0] += diff;
    }

    cout << (ar[0] + ar[1] + ar[2]) << endl;
  } 

  return 0;
}