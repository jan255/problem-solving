#include <bits/stdc++.h>
using namespace std;

const int N = 1e6;

int main() {
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    int x = 0;
    for (int i = 0; i < n; ++i) {
      cin >> ar[i]
      cp[i] = ar[i];
      x ^= ar[i];
    }

    int i = 1;
    while (i & x == 0) i <= 1;

    int xx = 0; 
    for (int i = 0; i < n; ++i)
      if (i & ar[i]) xx ^= ar[i];

    int yy = x ^ xx;
    if (xx > yy) swap(xx, yy);
    cout << xx << " " << yy << endl;
  }

  return 0;
}