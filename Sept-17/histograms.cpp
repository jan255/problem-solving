#include <bits/stdc++.h>
using namespace std;

const int N = 1e5;
int h[N];
int n;
int st[4 * N];
const long long inf = 1e15;

void build(int i = 0, int l = 0, int r = n - 1) {
  if (l == r) {
    st[i] = l;
    return;
  }
  int mid = (l + r) >> 1;
  build(2 * i + 1, l, mid);
  build(2 * i + 2, mid + 1, r);
  st[i] = h[st[2 * i + 1]] <= h[st[2 * i + 2]] ?
              st[2 * i + 1] : st[2 * i + 2]; 
}

int minQuery(int x, int y, int i = 0, int l = 0, int r = n - 1) {
  if (x > r || y < l) return -1;
  if (x <= l && y >= r) return st[i];
  int mid = (l + r) >> 1;
  int a = minQuery(x, y, 2 * i + 1, l, mid);
  int b = minQuery(x, y, 2 * i + 2, mid + 1, r);
  if (a == -1) return b;
  if (b == -1) return a;
  return h[a] <= h[b] ? a : b;
}

long long dnc(int i, int j) {
  if (i > j) return -inf;
  if (i == j) return h[i];
  int k = minQuery(i, j);
  long long subSol = (j - i + 1) * 1LL * h[k];
  return max(subSol, max(dnc(i, k - 1), dnc(k + 1, j)));
}

int main() {
  while (1) {
    cin >> n;
    if (!n) break;

    for (int i = 0; i < n; ++i)
      cin >> h[i];
    build();

    cout << dnc(0, n - 1) << endl;
  } 

  return 0;
}