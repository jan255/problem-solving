#include <bits/stdc++.h>
using namespace std;

string s;

int getRem(int exp) {
  if (exp == 0) return 1;
  int halfRem = getRem(exp / 2);
  return (halfRem * halfRem * (exp % 2 ? 2 : 1)) % 3;
}

bool isMult3() {
  reverse(s.begin(), s.end());
  int sumRems = 0;
  for (int i = 0; i < s.length(); ++i) {
    if (s[i] == '1') sumRems += getRem(i);
  }
  return sumRems % 3 == 0;
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> s;
    cout << (isMult3() ? 1 : 0) << endl;
  } 

  return 0;
}