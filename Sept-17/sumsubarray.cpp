#include <bits/stdc++.h>
using namespace std;

const int N = 101;
vector<int> v(N);

void compute(int s, int n) {
  int c = v[0];
  for (int i = 0, j = 0; i < n && j < n; ) {
    if (c > s) {
      if (i == n - 1) break;

      if (i == j) {
        i++; j++;
        c = v[i];
      } else {
        c -= v[i];
        i++;
      }
    } else if (c < s) {
      if (j == n - 1) break;

      j++;
      c += v[j];
    }

    if (c == s) {
      cout << ++i << " " << ++j;
      return;
    }
  }
  cout << -1;
}

int main() {
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    int s; cin >> s;
    for (int i = 0; i < n; ++i)
      cin >> v[i];  
    compute(s, n);
    if (T) cout << endl;
  }


  return 0;
}