#include <bits/stdc++.h>
using namespace std;


int main() {
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    for (int i = 0; i < n; ++i) {
      cin >> ar[i];
    }
    sort(ar, ar + n);

    int found = 0;
    for (int i = 0; i < n; ++i) {
      int s = -1 * ar[i];
      int l = i + 1, r = n - 1;
      while (l < r) {
        if (ar[l] + ar[r] > s) r--;
        else if (ar[l] + ar[r] < s) l++;
        else {
          found = 1;
        }
      }
    }
    cout << found << endl;
  } 

  return 0;
}