#include <bits/stdc++.h>
using namespace std;

const int N = 123;
int DP[N][N];
string s;
cin >> k;

int dp(int st, int en) {
  if (st >= en) return 0;
  if (DP[st][en] != -1) return DP[st][en];
  DP[st][en] = 1 + min(dp(st + 1, en), dp(st, en - 1));
  if (s[st] == s[en]) DP[st][en] = min(DP[st][en], dp(st + 1, en - 1));
  return DP[st][en];
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> s; cin >> k;
    for (int i = 0; i < s.length(); ++i)
      for (int j = 0; j < s.length(); ++j)
        DP[i][j] = -1;
    cout << (int)dp(0, s.length() - 1) << endl;
  } 

  return 0;
}