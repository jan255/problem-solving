#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int b[N];
bool bused[N];
int p[N];
bool pused[N];
int n, m, a;
int boughtAffordable = 0;
int maxBuyShared = 0;
vector<int> bi;

void buyAffordable() {
  int lastm = m - 1;
  for (int i = n - 1; i >= 0; --i) {
    int l = 0, r = lastm;
    while (l < r) {
      int mid = (l + r + 1) >> 1;
      if (p[mid] < b[i]) l = mid;
      else r = mid - 1;
    }
    if (p[l] > b[i]) l--;
    else {
      bi.push_back(i);
      bused[i] = true;
      pused[l] = true;
      boughtAffordable++;
    }
    lastm = l - 1;
  }

  sort(bi.begin(). bi.end());
}

void findHowManyWithShared() {
  int rem = n - boughtAffordable;
  int avail = a;
  for (int i = 0; i < m; ++i) {
    if (!pused[i] && avail >= p[i]) {
      maxBuyShared++;
      rem--;
      avail -= p[i];
    }

    if (rem == 0) return;
  }
}

void optimalPersonal() {
  for (int i = bi.size() - 1; i >= 0; --i)
    bi[i] += bi[i + 1];

  int bought = 0;
  int bidx = 0;
  for (int i = 0; i < m; ++i) {
    
  }
}

int main() {
  cin >> n >> m >> a;
  for (int i = 0; i < n; ++i)
    cin >> b[i];
  sort(b, b + n);
  for (int i = 0; i < m; ++i)
    cin >> p[i];
  sort(p, p + n);
  
  buyAffordable();
  findHowManyWithShared();

  return 0;
}