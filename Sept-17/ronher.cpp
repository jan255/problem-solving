#include <bits/stdc++.h>
using namespace std;

const int N = 123;
const int inf = 1e9 + 7;
string g[N];
bool vis[N][N];
int n, m;
int k;

int dfs(int x, int y, int gs = 0) {
  vis[x][y] = true;
  if (g[x][y] == '*') return gs;
  vector<pair<int, int> > adj;
  for (int i = -1; i < 2; ++i) {
    for (int j = -1; j < 2; ++j) {
      if (!i || !j) {
        int xx = x + i;
        int yy = y + j;
        if (xx >= 0 && xx < n && yy >= 0 && yy < m && !vis[xx][yy] && g[xx][yy] != 'X') {
          adj.push_back(make_pair(xx, yy));
        }
      }
    }
  }
  if (adj.size() == 0) return -inf;
  int add = adj.size() > 1;
  int mins = -1;
  for (int i = 0; i < adj.size(); ++i)
    mins = max(mins, dfs(adj[i].first, adj[i].second, gs + add));
  return mins;
}

int main() {
  int T;
  cin >> T;
  while (T--) {
    cin >> n >> m;
    int x = 0, y = 0;
    for (int i = 0; i < n; ++i) {
      cin >> g[i];
      for (int j = 0; j < m; ++j) {
        if (g[i][j] == 'M') {x = i; y = j;}
      }
    }
    cin >> k;
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
        vis[i][j] = false;

    cout << (dfs(x, y) == k ? "Impressed" : "Oops!") << endl;

  } 

  return 0;
}