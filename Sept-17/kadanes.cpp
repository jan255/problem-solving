#include <bits/stdc++.h>
using namespace std;


int main() {
  int T; cin >> T;
  while (T-- > 0) {
    int n; cin >> n;
    int globalMax = 0;
    int maxNow = 0;
    for (int i = 0; i < n; ++i) {
      int x; cin >> x;
      maxNow = max(x, x + maxNow);
      globalMax = max(globalMax, maxNow);
    }
    cout << globalMax << endl;
  } 

  return 0;
}