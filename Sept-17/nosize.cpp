#include <bits/stdc++.h>
using namespace std;

int main() {

  Listy l;
  
  int i = 1;
  while (l.elementAt(i) != -1) i = i << 1;

  int l = 0, r = i;
  int val = 0;
  while (l < r) {
    int mid = (l + r + 1) >> 1;
    if (l.elementAt(mid) == -1 || l.elementAt(mid) > val)
      r = mid - 1;
    else l = mid;
  }
  cout << l << endl;

  return 0;
}