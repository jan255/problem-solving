#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    vector<int> v(n);
    int s; cin >> s;

    for (int i = 0; i < n; ++i)
      cin >> v[i];

    int c = v[0];
    bool found = false;
    for (int l = 0, r = 1; r < n; ) {
      if (c > s) {
        c -= v[l++];
        if (l == r) r++;
      } else if (c < s) {
        c += v[r++];
      }

      if (c == s) {
        found = true;
        cout << l << " " << r - 1 << endl;
        break;
      }
    }
    if (!found) cout << -1 << endl;
  } 

  return 0;
}