#include <bits/stdc++.h>
using namespace std;

const int N = 1e6 + 1;
int a[N];
int b[N];
int ga[N];
int gb[N];

int main() {
  int n; cin >> n;
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    a[x]++;
  }
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    b[x]++;
  }

  for (int i = 1; i < N; ++i) {
    for (int j = i; j < N; j += i)
      if (a[j]) ga[i] = j;
  }
  for (int i = 1; i < N; ++i)
    for (int j = i; j < N; j += i)
      if (b[j]) gb[i] = j;

  int s = 0;
  for (int i = 1; i < N; ++i)
    if (ga[i] and gb[i]) s = max(ga[i], gb[j]);

  return 0;
}