#include <bits/stdc++.h>
using namespace std;

int n;
const int N = 2e5 + 1;
long long p[N];

int main() {
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> p[i];
  set<long long> st;
  st.insert(p[0]);
  long long ans = 1e16 + 1;
  for (int i = 1; i < n; ++i) {
    set<long long>::iterator ub = st.upper_bound(p[i]);
    if (ub != st.end()) {
      ans = min(ans, *ub - p[i]);
    }
    st.insert(p[i]);
  }
  cout << ans << endl;

  return 0;
}