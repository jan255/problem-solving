
r, g, b = sorted(map(int, input().split()))
if r == g and g == b:
	print (r)
elif r == g:
	L = 0
	R = b // 4
	while L < R:
		mid = (L + R) // 2
		if b - mid * 4 - (r - mid) < 3:
			R = mid
		else: L = mid + 1
	r += L
	print (r)	
elif g == b:
	g -= r
	b -= r
	r += g // 3
	if g % 3 > 1: r += 1
	print (r)
else:
	g -= r
	b -= r
	while g + b > 2 and g > 0 and b > 0:
		if g > b: g, b = b, g
		l = 1
		rr = b // 2
		while l < rr:
			mid = (l + rr) // 2
			if b - mid * 2 >= g - mid:
				rr = mid 
			else:
				l = mid + 1
		r += l
		g -= l
		b -= 2 * l
	print (r)

