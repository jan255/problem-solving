#include <bits/stdc++.h>
using namespace std;

const int N = 1e5;
int pi[N]; // LEN of longest proper prefix which is suffix

void preprocess(string& s) {
  pi[0] = -1;
  for (int i = 1; i < s.length(); ++i) {
    int j = i - 1;
    while (pi[j] >= 0 and s[pi[j]] != s[i])
      j = pi[pi[j] - 1];

    pi[i] = pi[j] + 1;
  }
}

void match(string& text, string& pat) {
  preprocess(pat);

  for (int i = 0, j = 0; i < text.length(); ) {
    if (text[i] == pat[j]) { i++; j++; }
    else {
      if (j == 0) { i++; }
      else {
        j = j > 0 ? pi[j] : 0;
      }
    }
    
    if (j == pat.length()) {
      cout << "MATCH: " << i - j << endl;
      j = j > 1 ? pi[j - 1] : 0;
    }
    // cout << i << " " << j << endl; 
  }

  cout << endl;
}

int main() {
  string text = "wyeroqassdsssassaaasaweuruiassdsssassaaaswoiroeassdsssassaaasweiorwero";
  string pat = "as";

  for (int i = 0; i < text.length(); ++i) {
    cout << i << " " << text[i] << endl;
  }

  match(text, pat);  

  for (int i = 0; i < pat.length(); ++i)
    cout << setw(3) << pi[i];
  cout << endl;

  return 0;
}