#include <bits/stdc++.h>
using namespace std;

const int N = 1001;
int suff[N];
string s;

struct Tuple {
  int left;
  int right;
  int idx;
};

bool comp(const struct Tuple& a, const struct Tuple& b) {
  if (a.left == b.left) return a.right < b.right;
  return a.left < b.left;
}

void buildSuff() {
  for (int i = 0; i < s.length(); ++i)
    suff[i] = s[i] - 'A';

  vector<Tuple> v(s.length());
  for (int i = 1; i <= s.length(); i <<= 1) {
    for (int j = 0; j < s.length(); ++j) {
      v[j].left = suff[j];
      v[j].right = (j + i < s.length()  ? suff[j + i] : -1);
      v[j].idx = j;
    }

    sort(v.begin(), v.end(), comp);

    int r = 0;
    suff[v[0].idx] = r;
    for (int j = 1; j < s.length(); ++j) {
      if (v[j].left != v[j - 1].left || v[j].right != v[j - 1].right) {
        suff[v[j].idx] = ++r;
      } else {
        suff[v[j].idx] = r;
      }
    }
  }

  for (int i = 0; i < s.length(); ++i)
    suff[i] = v[i].idx;
}

int compute() {
  buildSuff();
  int disubs = s.length() - suff[0];
  for (int i = 1; i < s.length(); ++i) {
    int j = 0;
    while (suff[i - 1] + j < s.length() && suff[i] + j < s.length() && 
      s[suff[i - 1] + j] == s[suff[i] + j])
      ++j;
    disubs += s.length() - suff[i] - j;
  }
  return disubs;
}

int main() {
  int T; cin >> T;
  while (T--) {
    cin >> s;
    cout << compute() << endl;
  } 

  return 0;
}