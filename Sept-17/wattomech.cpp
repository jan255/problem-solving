#include <bits/stdc++.h>
using namespace std;

const int N = 1e6;
string sor[N];
int n, m;

int comp(string& a, string& b) {
  int i, j;
  for (i = 0, j = 0; i < a.length() && j < b.length(); ++i, ++j) {
    if (a[i] < a[j]) return -1;
    if (a[i] > a[j]) return 1;
  }
  if (i == a.length() && j == b.length())
    return 0;
  return i == a.length() ? -1 : 1;
}

bool yes(string& a) {
  int l = 0, r = n - 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (comp(sor[mid], a) == -1) l = mid + 1;
    else r = mid;
  }
  return comp(sor[l], a) == 0;
}

char add(char c, int j) {
  return 'a' + (c - 'a' + j) % 26;
}

int main() {
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> sor[i];
  sort(sor, sor + n);

  while (m--) {
    string s; cin >> s;
    bool res = false;
    for (int i = 0; i < s.length(); ++i) {
      for (int j = 1; j < 26; ++j) {
        s[i] = add(s[i], 1);
        res = res || yes(s);
      }
      if (res) break;
      s[i] = add(s[i], 1);
    }
    cout << (res ? "YES" : "NO") << endl;
  }

  return 0;
}