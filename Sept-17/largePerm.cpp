#include <bits/stdc++.h>
using namespace std;


int main() {
  int n, k;
  cin >> n >> k;
  vector<int> v(n);
  set<pair<int, int> > st;
  for (int i = 0; i < n; ++i) {
    cin >> v[i];
    st.insert(make_pair(v[i], i));
  }

  set<pair<int, int> >::iterator it = st.end();

  
  for (int i = 0; i < n and k > 0; ++i) {
    it = --st.end();
    int x = it->first;
    int maxi = it->second;
    st.erase(make_pair(v[i], i));
    st.insert(make_pair(v[i], maxi));
    st.erase(it);
    v[maxi] = v[i];
    v[i] = x;

    if (v[i] == x) continue;
    else {
      --k;
    }
  }

  for (int i = 0; i < n; ++i)
    cout << v[i] << " ";



  return 0;
}