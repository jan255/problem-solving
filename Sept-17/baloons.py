
def bs(a, b):
  if a + b < 3 or a < 1 or b < 1: return 0
  if a < b: a, b = b, a
  # print ('*', a, b)
  x = 1
  l = 1
  r = min(a // 2, b)
  while l < r:
  	# print (' ', l, r)
  	mid = (l + r) // 2
  	if a - mid * 2 <= b - mid:
          r = mid
  	else: l = mid + 1
  print (' ', a, b)
  return l + bs(a - l * 2, b - l)

r, g, b = sorted(map(int, input().split()))
g -= r
b -= r
print (r + bs(g, b))
