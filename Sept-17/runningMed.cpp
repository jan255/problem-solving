#include <bits/stdc++.h>
using namespace std;

priority_queue<int, vector<int>, lesser<int> > l;
priority_queue<int, vector<int>, greater<int> > r;

void balance() {
  while ((l.size() + r.size()) % 2 == 0 && r.size() > l.size()) {
    l.push(r.top());
    r.pop();
  }
}

double median() {
  double sums = 0;
  if ((l.size() + r.size()) % 2 == 0)
    sums = (l.top() + 1.0 + r.top()) / 2.0;
  else sums += r.top();
  return sums;
}

int main() {
  int n; cin >>n;
  while (n--) {
    int x; cin >> x;
    r.push(x);
    balance();
    cout << median() << endl;
  }

  return 0;
}