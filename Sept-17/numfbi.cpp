#include <bits/stdc++.h>
using namespace std;


int main() {
  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    string s; cin >> s;
    bool invalid = false;
    for (int i = 0; i < n - 1; ++i) {
      if (s[i] == s[i - 1] && s[i] == '0')
        invalid = true;
    }
    if (invalid) {
      cout << 0 << endl;
      continue;
    }
    vector<int> dp(n, 0);
    dp[n - 1] = s[n - 1] != '0';
    for (int i = n - 2; i >= 0; --i) {
      dp[i] = 0;
      dp[i] = (s[i] != '0' ? dp[i + 1] : 0);
      int dig = (s[i] - '0') * 10 + s[i + 1] - '0';
      if (s[i] != '0' && dig <= 26 && dig >= 1)
        dp[i] += (i + 2 < n ? d[i + 2] : 1);
    }

    cout << dp[0] << endl;
  } 

  return 0;
}