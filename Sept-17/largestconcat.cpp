#include <bits/stdc++.h>
using namespace std;

const int N = 123;
string a[N];

bool comp(const string& a, const string& b) {
  vector<int> t;
  for (int i = 0; i < a.length(); ++i)
    t.push_back(a[i]);
  for (int i = 0; i < b.length(); ++i)
    t.push_back(b[i]);
  vector<int> f;
  for (int i = 0; i < b.length(); ++i)
    f.push_back(b[i]);
  for (int i = 0; i < a.length(); ++i)
    f.push_back(a[i]);
  for (int i = 0; t.size(); ++i) {
    if (t[i] > f[i]) return true;
    if (t[i] < f[i]) return false;
  }
  return true;
}

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    for (int i = 0; i < n; ++i)
      cin >> a[i];
    sort(a, a + n, comp);

    for (int i = 0; i < n; ++i)
      cout << a[i];
    if (t) cout << endl;
  } 

  return 0;
}