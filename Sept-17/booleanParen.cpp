#include <bits/stdc++.h>
using namespace std;

const int N = 123;
const int MOD = 1003;
int DP[2][N][N];
int n;
string s;

int dp(int i, int j, int b) {
  if (i == j) return b ? s[i] == 'T' : s[i] == 'F';
  if (DP[b][i][j] != -1) return DP[b][i][j];
  DP[b][i][j] = 0;
  switch (s[i + 1]) {
    case '&':
      if (s[i] == 'T') DP[b][i][j] = dp(i + 2, j, 1);
      break;
    case '|':
      if (s[i] == 'T') DP[b][i][j] = (dp(i + 2, j, 0) + dp(i + 2, j, 1)) % MOD;
      else DP[b][i][j] = dp(i + 2, j, 1);
      break;
    case '^':
      if (s[i] == 'T') DP[b][i][j] = dp(i + 2, j, 0);
      else DP[b][i][j] = dp()
  }
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> n >> s;
    for (int b = 0; b < 2; ++b)
      for (int i = 0; i < n; ++i)
        for (int j = 0; j < n; ++j)
          DP[b][i][j] = -1;

    cout << dp(0, n - 1, 1) << endl;
  } 

  return 0;
}