#include <bits/stdc++.h>
using namespace std;

int h[1234];
int n;
const int inf = 1e9 + 7;

int getMinIdx(int a, int b) {
  int minIdx = a;
  for (int i = a + 1; i <= b; ++i) {
    if (h[minIdx] > h[i]) minIdx = i;
  }
  return minIdx;
}

int query(int a, int b) {
  if (a > b) return -inf;
  if (a == b) return h[a];
  int minIdx = getMinIdx(a, b);
  int partialSol = h[minIdx] * (b - a + 1);
  return max(partialSol, max(query(a, minIdx - 1), query(minIdx + 1, b)));
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> n;
    for (int i = 0; i < n; ++i) {
      cin >> h[i];
    }
    cout << query(0, n - 1) << endl;
  } 

  return 0;
}