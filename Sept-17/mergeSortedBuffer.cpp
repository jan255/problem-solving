#include <bits/stdc++.h>
using namespace std;

const int N = 10;
int a[N];

int bs1() {
  int l = 0, r = N - 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (a[mid] > a[0]) l = mid + 1;
    else r = mid;
  }
  return l;
}

int main() {

  for (int i = 0; i < N; ++i) {
    a[(i + 4) % N] = i;
  }
  for (int i = 0; i < N; ++i)
    cout << a[i] << " ";
  cout << endl;

  int times = bs1();
  cout << times << endl;

  int val = 3;
  int l = 0, r = N - 1;
  while (l < r) {
    cout << l << " " << r << endl;
    int mid = (l + r) >> 1;
    mid = (mid + times) % N;
    if (a[mid] == val) {
      cout << mid << endl;
      break;
    }
    if (a[mid] < val) l = (mid + 1 - times + N) % N;
    else r = (mid - 1 - times + N) % N;
  }
  cout << l + times << endl;

  return 0;
}