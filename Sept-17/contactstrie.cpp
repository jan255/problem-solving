#include <bits/stdc++.h>
using namespace std;

const int N = 3e6;

struct node {
  int idx[26] = {0};
  int c = 0;
};

struct node trie[N];
int f = 1;

void add(string& s) {
  int j = 0;
  for (int i = 0; i < s.length(); ++i) {
    int c = s[i] - 'a';
    if (trie[j].idx[c] != 0) {
      j = trie[j].idx[c];
    } else {
      trie[j].idx[c] = f;
      j = f++;
    }
    trie[j].c++;
  }
}

int getCount(string& s) {
  int j = 0;
  for (int i = 0; i < s.length(); ++i) {
    int c = s[i] - 'a';
    if (trie[j].idx[c] == 0) return 0;
    j = trie[j].idx[c];
  }
  return trie[j].c;
}

int main() {
  int n; cin >> n;
  while (n--) {
    string op; cin >> op;
    string s; cin >> s;
    if (op == "add") {
      add(s);
    } else {
      getCount(s);
    }
  } 

  return 0;
}