#include <bits/stdc++.h>
using namespace std;

const int N = 10;
char grid[N][N];
bool vis[N][N];

int n, m;
set<string> strs;
vector<string> foundWords;

void dfs(int x, int y, string prev = "") {
  if (vis[x][y] || x < 0 || y < 0 || x > n || y > m) return;
  vis[x][y] = true;
  string s = prev + grid[x][y];
  if (strs.find(prev) != strs.end())
    foundWords.push_back(s);
  for (int i = -1; i < 2; ++i) {
    for (int j = -1; j < 2; ++j) {
      if (i != 0 || j != 0) {
        dfs(x + i, y + j, s);
      }
    }
  }
  vis[x][y] = false;
}

void compute() {
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      vis[i][j] = false;

  foundWords.clear();
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      dfs(i, j);

  for (int i = 0; i < foundWords.size(); ++i)
    cout << foundWords[i] << " ";
  if (foundWords.empty()) cout << -1;
  cout << endl;
}

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    strs.clear();
    while (n--) {
      string str; cin >> str;
      strs.insert(str);
    }

    cin >> n >> m;
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < m; ++j)
        cin >> grid[i][j];
    compute();
  } 

  return 0;
}