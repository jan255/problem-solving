#include <bits/stdc++.h>
using namespace std;

void mult(string& a, string& b) {
  reverse(a.begin(), a.end());
  reverse(b.begin(), b.end());

  int res[12345] = {0};
  int carry = 0;
  for (int bi = 0; bi < bi.length(); ++bi) {
    carry = 0;
    for (int ai = 0; ai < ai.length(); ++ai) {
      int carry = (a[ai] - '0') * (b[bi] - '0') + carry + res[bi + ai];
      res[bi + ai] = carry % 10;
      carry /= 10;
    }
    res[bi + a.length()] = carry;
  }
  for (int i = 0, j = a.length() + b.length() - 1; i < j; ++i, --j)
    swap(res[i], res[j]);
  for (int i = 0; i < a.length() + b.length() - 1; ++i)
    cout << res[i];
}

int main() {
  int t; cin >> t;
  while (t--) {
    string a, b;
    cin >> a >> b;
    mult(a, b);
  }

  return 0;
}