#include <bits/stdc++.h>
using namespace std;

string s;
const int N = 1e5 + 1;
const long long MOD = 1e9 + 7;
long long sols[N];

long long numSubs(long long n) {
  return (n * ((n + 1) / 2)) % MOD;
}

// sum, lastSum
pair<long long, long long> rec(int n) {
  if (n == 0) return make_pair(s[0] - '0', s[0] - '0');
  pair<long long, long long> subSol = rec(n - 1);
  long long lastSum = (subSol.second * 10 + (n + 1) * (s[n] - '0')) % MOD;
  long long sum = (subSol.first + lastSum) % MOD;
  return make_pair(sum, lastSum);
}

int main() {
  cin >> s; 
  cout << rec(s.length() - 1).first << endl;

  return 0;
}