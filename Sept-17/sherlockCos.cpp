#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int b[N];
int n;

int main() {
  int T; cin >> T;
  while (T--) {
    cin >> n;
    for (int i = 0; i < n; ++i) {
      cin >> b[i];
    }
    if (n == 1) {
      cout << 0 << endl;
      continue;
    }

    int x = b[0] - 1;
    int y = max(b[1] - 1, abs(b[1] - b[0]));

    for (int i = 2; i < n; ++i) {
      int xx = y + b[i - 1] - 1;
      int yy = max(x + b[i] - 1, y + abs(b[i] - b[i - 1]));
      x = xx;
      y = yy;
    }
    cout << max(x, y) << endl;
  }

  return 0;
}