#include <bits/stdc++.h>
using namespace std;

const int N = 33;
int mat[N][N];
int n, m;
int f;

int searchMat() {
  int i = n - 1, j = 0;
  while (i >= 0 && j < m) {
    if (mat[i][j] == f)
      return 1;
    if (mat[i][j] > f) --i;
    else if (mat[i][j] < f) ++j;
  }
  return 0;
}

int main() {
  int t; cin >> t;
  while (t-- > 0) {
    cin >> n >> m;
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < m; ++j)
        cin >> mat[i][j];
    cin >> f;
    cout << searchMat() << endl;
  } 

  return 0;
}