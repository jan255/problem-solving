#include <bits/stdc++.h>
using namespace std;


int main() {
  vector<int> v(1000);
  vector<int> idx(1000);

  int T; cin >> T;
  while (T--) {
    int n; cin >> n;
    for (int i = 0; i < n; ++i) {
      cin >> v[i];
      idx[i] = i;
    }
    sort(idx.begin(), idx.end(), [v](const int& a, const int& b){
      return v[a] < v[b];
    });

    int maxDiff = 0;
    int minSoFar = idx[0];
    for (int i = 1; i < n; ++i) {
      maxDiff = max(maxDiff, idx[i] - minSoFar);
      minSoFar = min(minSoFar, idx[i]);
    }

    cout << maxDiff << endl;
  } 

  return 0;
}