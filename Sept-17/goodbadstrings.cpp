#include <bits/stdc++.h>
using namespace std;

const int N = 6e6+ 6;
string s[N];

struct node {
  int idx[7] = {0};
  int c = 0;
};

struct node trie[N];
int f = 1;

void add(string& s) {
  int j = 0;
  for (int i = 0; i < s.length(); ++i) {
    int c = s[i] - 'a';
    if (trie[j].idx[c] != 0) j = trie[j].idx[c];
    else {
      trie[j].idx[c] = f;
      j = f++;
    }
  }
  trie[j].c++;
}

int dfs(int ti = 0) {
  for (int i = 0; i < 7; ++i) {
    if (trie[ti].idx[i] > 0)
      trie[ti].c += dfs(trie[ti].idx[i]);
  }
  return trie[ti].c;
}

bool prefixFound(string& s) {
  int j = 0;
  for (int i = 0; i < s.length(); ++i) {
    int c = s[i] - 'a';
    j = trie[j].idx[c];
  }
  return trie[j].c > 1;
}

int main() {
  int n; cin >> n;
  for (int i = 0; i < n; ++i) {
    cin >> s[i];
    add(s[i]);
  }
  dfs();

  for (int i = 0; i < n; ++i) {
    if (prefixFound(s[i])) {
      cout << "BAD SET" << endl << s[i] << endl;
      return 0;
    }
  }
  cout << "GOOD SET";

  return 0;
}