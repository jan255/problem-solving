#include <bits/stdc++.h>
using namespace std;

const int N = 8;
int board[N][N];

bool validPlacement(int row, int col) {
  for (int i = 0; i < N; ++i) {
    if (i != row && board[i][col] == 1)
      return false;
    if (i != col && board[row][i] == 1)
      return false;
    for (int j = -1; j < 2; ++j) {
      for (int k = -1; k < 2; ++k) {
        if (i != 0 && k != 0) {
          int x = row + i * j;
          int y = col + i * k;
          if (x >= 0 && y >= 0 && x < N && y < N && board[x][y] == 1)
            return false;
        }
      }
    }
  }
  return true;
}

void printBoard() {
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j)
      cout << board[i][j];
    cout << endl;
  }
  cout << endl;
}

void place(int row = 0) {
  if (row == N) {
    printBoard();
    return;
  }
  for (int i = 0; i < N; ++i) {
    board[row][i] = 1;
    if (validPlacement(row, i)) place(row + 1);
    board[row][i] = 0;
  }
}

int main() {
  place();

  return 0;
}