#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t--) {
    string s; cin >> s;
    stack<int> st;
    for (int i = 0; i < s.length(); ++i) {
      if (s[i] == ')' && !st.empty() && s[st.top()] == '(') {
        st.pop();
      } else {
        st.push(i);
      }
    }
    int maxLen = 0;
    int j = n;
    while (!st.empty()) {
      int i = st.top();
      st.pop();
      maxLen = max(maxLen, j - i - 1);
      j = i;
    }
    maxLen = max(maxLen, j);

    cout << maxLen << endl;
  } 

  return 0;
}