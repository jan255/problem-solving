
def bs(a, b):
    if a + b < 3: return 0
    if a < b: a, b = b, a
    x = 1
    l = 1, r = 10 ** 9
    while l < r:
        mid = (l + r) / 2
        if a - mid * 2 < b - mid:
            r = mid
        else: l = mid + 1
    return l * 2 + bs(a - l * 2, b - l)

r, g, b = sorted(map(int, input().split()))
g -= r
b -= r
print (r + bs(g, b))
