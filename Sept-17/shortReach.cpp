#include <bits/stdc++.h>
using namespace std;

const int N = 3001;
vector<pair<int, int> > g[N];
int dist[N];

int n, m;
int s;

void compute() {
  for (int i = 1; i <= n; ++i)
    dist[i] = -1;
  dist[s] = 0;
  priority_queue<pair<int, int>, vector<pair<int, int> >, greater<pair<int, int> > > pq;
  pq.push(make_pair(0, s));
  while (!pq.empty()) {
    pair<int, int> p = pq.top(); pq.pop();
    int v = p.first;
    int w = p.second;
    for (int i = 0; i < g[v].size(); ++i) {
      int u = g[v][i];
      if (dist[u] == -1 || dist[u] > dist[v] + w) {
        dist[u] = dist[v] + w;
        pq.push(make_pair(dist[u], u));
      }
    }
  }
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> n >> m;
    for (int i = 1; i <= n; ++i)
      g[i].clear();
    while (m--) {
      int u, v;
      cin >> u >> v >> w;
      g[u].push_back(make_pair(v, w));
    }
    compute();
    for (int i = 1; i <= n; ++i)
      cout << dist[i] << " ";
    cout << endl;
  } 

  return 0;
}