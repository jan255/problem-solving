#include <bits/stdc++.h>
using namespace std;

const int N = 123;
long long DP[2][N][N];
vector<string> s;

void tokenize(string& line) {
  s.clear();
  int st = 0, en = -1;
  while (line.find(' ', en + 1) != string::npos) {
    st = en + 1;
    en = line.find(' ', st);
    s.push_back(line.substr(st, en - st));
  }
  s.push_back(line.substr(en + 1));
}

long long dp(int i, int j, int b) {
  if (i == j) return s[i] == "T" ? b == 1 : b == 0;
  if (DP[b][i][j] != -1) return DP[b][i][j];
  DP[b][i][j] = 0;
  for (int k = i + 1; k < j; ++k) {
    string& op = s[k];
    long long l0 = dp(i, k - 1, 0);
    long long l1 = dp(i, k - 1, 1);
    long long r0 = dp(k + 1, j, 0);
    long long r1 = dp(k + 1, j, 1);
    if (op == "or") {
      if (b == 1) {
        DP[b][i][j] += l0 * 1LL * r1 + 
                  l1 * 1LL * r0 + l1 * 1LL * r1;
      } else {
        DP[b][i][j] += l0 * 1LL * r0;
      }
    } else if (op == "and") {
      if (b == 1) {
        DP[b][i][j] += l1 * 1LL * r1;
      } else {
        DP[b][i][j] += l0 * 1LL * r1 + l1 * 1LL * r0 + l0 * 1LL * r0;
      }
    } else if (op == "xor") {
      if (b == 1) {
        DP[b][i][j] += l1 * 1LL * r0 + l0 * 1LL * r1;
      } else {
        DP[b][i][j] += l0 * 1LL * r0 + l1 * 1LL * r1;
      }
    }
  }

  return DP[b][i][j];
}

int main() {
  int t; cin >> t;
  string line;
  getline(cin, line);
  
  while (t--) {
    getline(cin, line);
    tokenize(line);
    for (int i = 0; i < 2; ++i)
      for (int j = 0; j < N; ++j)
        for (int k = 0; k < N; ++k)
          DP[i][j][k] = -1;
    cout << dp(0, s.size() - 1, 1) << endl;
  }

  return 0;
}