#include <bits/stdc++.h>
using namespace std;

const int N = 356;
int req[N];
int h[N][N];
int sums[N][N];
int n, m, k;

int getSum(int a, int b, int x, int y) {
  return sums[x][y] + ((a && b) ? sums[a - 1][b - 1] : 0)
        - (b ? sums[x][b - 1] : 0) - (a ? sums[a - 1][y] : 0);
}

bool notOk(int x, int y, int ht) {
  for (int i = x - ht + 1; i < x + ht - 1; ++i) {
    for (int j = y - ht + 1; j < y + ht - 1; j++) {
      int maxh = ht - max(abs(i - x), abs(j - y));
      if (h[i][j] > maxh) return true;
    }
  }
  return false;
}

bool isPos(int x, int y, int ht) {
  if (notOk(x, y, ht)) return false;
  int now = getSum(x - ht + 1, y - ht + 1, x + ht - 1, y + ht - 1);
  return req[ht] - now <= k; 
}

int findMax(int x, int y) {
  int maxh = min(min(x + 1, y + 1), min(n - x, m - y));
  int pH = 0;
  for (int i = h[x][y]; i <= maxh; ++i) {
    if (isPos(x, y, i)) {
      // cout << x << " " << y << " " << i << endl;
      pH = i;
    }
  }
  return pH;
}

int main() {
  req[1] = 1;
  for (int i = 2; i < N; ++i)
    req[i] = (2 * i - 1) * (2 * i - 1) + req[i - 1];

  int q; cin >> q;
  while (q--) {
    cin >> n >> m;
    cin >> k;
    for (int i = 0; i < n; ++i)
      for (int j = 0; j < m; ++j)
        cin >> h[i][j];

    for (int i = 0; i < n; ++i) {
      int rowSum = 0;
      for (int j = 0; j < m; ++j) {
        rowSum += h[i][j];
        sums[i][j] = rowSum + (i ? sums[i - 1][j] : 0); 
      }
    }

    int maxh = 0;
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        maxh = max(maxh, findMax(i, j));
      }
    }
    cout << maxh << endl;
  }

  return 0;
}