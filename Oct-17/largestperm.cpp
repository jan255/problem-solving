#include <bits/stdc++.h>
using namespace std;


int main() {
  int n, k;
  cin >> n >> k;
  vector<int> v(n);
  vector<int> idx(n + 1);
  for (int i = 0; i < n; ++i) {
    cin >> v[i];
    idx[v[i]] = i;
  }

  for (int i = 0; k > 0 && i < n; ++i) {
    if (v[i] == n - i) continue;
    swap(v[i], v[idx[i]]);
    swap(idx[v[i]], idx[v[idx[i]]]);
  }

  for (int i = 0; i < n; ++i)
    cout << v[i] << " ";
  cout << endl;


  return 0;
}