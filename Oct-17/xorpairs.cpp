#include <bits/stdc++.h>
#include <unordered_set>
using namespace std;


int main() {
  int n; cin >> n;
  unordered_set<int> allnums;
  vector<int> xx(n);
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    xx[i] = x;
    allnums.insert(x);
  }
  vector<int> yy(n);
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    yy[i] = x;
    allnums.insert(x);
  }

  int numpairs = 0;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < n; ++j) {
      numpairs += allnums.count(xx[i] ^ yy[j]);
    }
  }
  cout << (numpairs % 2 ? "Koyomi" : "Karen") << endl;



  return 0;
}