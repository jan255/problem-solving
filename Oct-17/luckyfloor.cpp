
const int N = 12;
// idx, 4s, 13s, prev, same
int DP[N][N][N][N][N];

vector<int> digs;

int dp(int di, int f, int t, int prev, int same) {
  if (di >= digs.size()) {
    return f + t > 0;
  }
  if (DP[di][f][t][prev][same] != -1) {
    return DP[di][f][t][prev][same];
  }

  int sol = 0;
  for (int i = 0; i < 10; ++i) {
    if (same) {
      if (i <= digs[di]) {
        sol += dp(di + 1, f + (i == 4), t + (prev == 1 && i == 3), i, same && (i == digs[di]));
      }
    } else {
      sol += dp(di + 1, f + (i == 4), t + (prev == 1 && i == 3), i, same);
    }
  }

  return DP[di][f][t][prev][same] = sol;
}

int countFloors(int n) {
  int tmpn = n;
  digs.clear();
  while (n > 0) {
    digs.push_back(n % 10);
    n /= 10;
  }
  reverse(digs.begin(), digs.end());

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int k = 0; k < N; ++k)
        for (int l = 0; l < N; ++l)
          for (int m = 0; m < N; ++m)
            DP[i][j][k][l][m] = -1;

  return tmpn - dp(0, 0, 0, 0, 1);
}

// binary search + dynamic programming
// roughly 1e6 steps for any given INT
int getLuckyFloorNumber(int n) {
  int l = 1, r = INT_MAX;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (countFloors(mid) < n) l = mid + 1;
    else r = mid;
  }
  return l;
}