#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;


int main() {
  ios::sync_with_stdio(false);

  long long t; cin >> t;
  while (t--) {
    long long n; cin >> n;
    vector<long long> v(n);
    for (long long i = 0; i < n; ++i) {
      cin >> v[i];
    }

    long long bestSum = v[0];
    long long sumNow = v[0];
    for (long long i = 1; i < n; ++i) {
      sumNow = max(v[i], v[i] + sumNow);
      bestSum = max(bestSum, sumNow);

      v[i] += v[i - 1];

    }

    unordered_map<long long, long long> counts;
    counts[0] = 1;
    long long numSeq = 0;
    for (long long i = 0; i < n; ++i) {
      numSeq += counts[v[i] - bestSum];
      counts[v[i]]++;
    }
    cout << bestSum << ' ' << numSeq << endl;
  }

  return 0;
}