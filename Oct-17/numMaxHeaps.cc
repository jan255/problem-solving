
const int N = 101;
const int MOD = 1e9 + 7;
int nr[N][N];

int DP[N];

int ncr(int n, int r) {
    if (n < r) return 0;
    if (n == 0 or r == 0) return 1;
    if (nr[n][r] != -1) return nr[n][r];
    return nr[n][r] = ncr(n - 1, r - 1) + ncr(n - 1, r);
}

pair<int, int> leftRightCounts(int n) {
    n--;
    int l = 0, r = 0;
    int i = 0;
    for ( ; l + r + (1 << (i + 1)) <= n; ++i) {
        l += (1 << i);
        r += (1 << i);
    }
    
    if (l + r + (1 << i) <= n) {
        l += (1 << i);
    } else {
        l += n - l - r;
    }
    
    if (l + r < n) {
        r += n - l - r;
    }    
        
    return make_pair(l, r);
}

int dp(int n) {
    if (n < 3) return 1;
    if (n < 4) return 2;
    if (DP[n] != -1) return DP[n];
    pair<int, int> counts = leftRightCounts(n);
    int sol = ncr(n - 2, counts.first) * dp(counts.first) * dp(n - 1 - counts.first);
    sol += ncr(n - 2, counts.second) * dp(counts.second) * dp(n - 1 - counts.second);
    return DP[n] = sol;
}

int Solution::solve(int n) {
    for (int i = 0; i < N; ++i)
        for (int j = 0; j < N; ++j)
            DP[j] = nr[i][j] = -1;

    return dp(n); 
}
