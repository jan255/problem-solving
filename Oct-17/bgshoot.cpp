#include <bits/stdc++.h>
using namespace std;

#define m2 (i << 1)

int n;
vector<pair<int, int> > animals;
vector<pair<int, int> > queries;

const int inf = 1e6 + 10;
const int N = 1e5 + 10;
int sums[N];
int seg[4 * N];

void build(int i = 0, int l = 0, int r = N - 1) {
  if (l == r) {
    seg[i] = sums[l];
    return;
  }
  int mid = (l + r) >> 1;
  build(m2 + 1, l, mid);
  build(m2 + 2, mid + 1, r);
  seg[i] = max(seg[m2 + 1], seg[m2 + 2]);
}

int getMax(int x, int y, int i = 0, int l = 0, int r = N - 1) {
  if (x > r || y < l) return -inf;
  if (x <= l && y >= r) return seg[i];
  int mid = (l + r) >> 1;
  return max(getMax(x, y, m2 + 1, l, mid),
              getMax(x, y, m2 + 2, mid + 1, r));
}

int main() {
  cin >> n;
  map<int, int> maper;
  for (int i = 0; i < n; ++i) {
    int l, r;
    cin >> l >> r;
    maper[l] = maper[r] = 0;
    animals.push_back(make_pair(l, r));
  }
  int q; cin >> q;
  while (q--) {
    int a, b;
    cin >> a >> b;
    maper[a] = maper[b] = 0;
    queries.push_back(make_pair(a, b));
  }

  int cnt = 1;
  for (map<int, int>::iterator it = maper.begin();
          it != maper.end(); ++it) {
    it->second = cnt++;
  }

  for (int i = 0; i < animals.size(); ++i) {
    animals[i].first = maper[animals[i].first];
    animals[i].second = maper[animals[i].second];
    sums[animals[i].second + 1] -= 1;
    sums[animals[i].first] += 1;
  }
  for (int i = 1; i < N; ++i) {
    sums[i] += sums[i - 1];
  }
  build();

  for (int i = 0; i < queries.size(); ++i) {
    queries[i].first = maper[queries[i].first];
    queries[i].second = maper[queries[i].second];
    cout << getMax(queries[i].first, queries[i].second) << endl;
  }

  return 0;
}