#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int DP[N][5];
int v[5];
int n, x;

int dp(int x, int n) {
  if (x < 0) return 0;
  if (n == 0) return x % v[n] == 0;
  if (DP[x][n] != -1) return DP[x][n];
  int s = 0;
  for (int i = 0; i <= n; ++i) s += v[i];
  return DP[x][n] = dp(x - s, n) || dp(x, n - 1);
}

int main() {
  cin >> x;
  cin >> n;
  for (int i = 0; i < n; ++i)
    cin >> v[i];

  memset(DP, -1, sizeof(int) * N * 5);

  cout << (!dp(x, n - 1) ? "NO" : "YES") << endl;


  return 0;
}