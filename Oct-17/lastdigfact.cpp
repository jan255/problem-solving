#include <bits/stdc++.h>
using namespace std;


int main() {
  long long a, b;
  cin >> a >> b;
  int lastDig = 1;
  for (int i = 1; i < 10; ++i) {
    if (a + i <= b) {
      lastDig = lastDig * ((a + i) % 10);
      lastDig = lastDig % 10;
    }
  } 
  cout << lastDig << endl;

  return 0;
}