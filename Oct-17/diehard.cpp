#include <bits/stdc++.h>
using namespace std;

map<pair<int, pair<int, int> >, int > mem;

int dx[3][2] = {
  {3, 2},
  {-5, -10},
  {-20, 5}
};

int dp(int h, int a, int p) {
  if (h <= 0 || a <= 0) return 0;
  if (mem.find(make_pair(p, make_pair(h, a))) != mem.end()) return mem[make_pair(p, make_pair(h, a))];
  int sol = 0;
  for (int i = 0; i < 3; ++i) {
    if (p != i) {
      sol = max(sol, 1 + dp(h + dx[i][0], a + dx[i][1], i));
    }
  }
  return mem[make_pair(p, make_pair(h, a))] = sol;
}

int main() {
  int t; cin >> t;
  while (t--) {
    int h, a;
    cin >> h >> a;
    cout << max(max(dp(h, a, 0), dp(h, a, 1)), dp(h, a, 2)) - 1 << endl;
  } 

  return 0;
}