#include <bits/stdc++.h>
using namespace std;

const int N = 8;
int dist[N][N][N][N];

bool three(int i, int j, int a, int b, int ia, int jb) {
  if ((i == ia && j == jb) || (a == ia && j == jb)) return false;
  return (dist[i][j][a][b] >= dist[i][j][ia][jb] + dist[ia][jb][a][b]);
}

int dx[8][2] = {
  {2, 1}, {2, -1},
  {-2, 1}, {-2, -1},
  {-1, 2}, {1, 2},
  {-1, -2}, {1, -2}
};

void floyd() {
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      for (int a = 0; a < N; ++a) {
        for (int b = 0; b < N; ++b) {
          dist[i][j][a][b] = 100000000;
          if (i == a && j == b) {
            dist[i][j][a][b] = 0;
          } else if ((abs(i - a) == 2 && abs(j - b) == 1) || 
                (abs(i - a) == 1 && abs(j - b) == 2)) {
            dist[i][j][a][b] = i * a + j * b;
          }
        }
      }
    }
  }
  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j) {
      for (int a = 0; a < N; ++a) {
        for (int b = 0; b < N; ++b) {
          if ((i == a && j == b)) continue;
          for (int ia = 0; ia < N; ++ia) {
            for (int jb = 0; jb < N; ++jb) {
              if (three(i, j, a, b, ia, jb)) {
                dist[i][j][a][b] = dist[i][j][ia][jb] + dist[ia][jb][a][b];
              }
            }
          }
        }
      }
    }
  }

  for (int i = 0; i < N; ++i) {
    for (int j = 0; j < N; ++j)
      cout << dist[0][0][i][j] << " ";
    cout << endl;
  }

}

int main() {
  floyd();   
  int a, b, c, d;
  // map<pair<int, int>, int > vis;
  while (cin >> a >> b >> c >> d) {
    d = dist[a][b][c][d];
    cout << (d >= 100000000 ? -1 : d) << endl;
    // if (a == c && b == d) {
    //   cout << 0 << endl;
    //   continue;
    // }
    // vis.clear();
    // vis[make_pair(a, b)] = 0;
    // queue<pair<int, int> > q;
    // q.push(make_pair(a, b));
    // while (!q.empty()) {
    //   pair<int, int> p = q.front();
    //   if (p.first == c && p.second == d) break;
    //   q.pop();
    //   for (int i = 0; i < 8; ++i) {
    //     int ii = p.first + dx[i][0];
    //     int jj = p.second + dx[i][1];
    //     if (!(ii < 0 || jj < 0 || ii >= 8 || jj >= 8) && vis.find(make_pair(ii, jj)) == vis.end()) {
    //       vis[make_pair(ii, jj)] = vis[make_pair(p.first, p.second)] + p.first * ii + p.second * jj;
    //       q.push(make_pair(ii, jj));
    //     }
    //   }
    // }
    // cout << vis[make_pair(c, d)] << endl;
  }

  return 0;
}