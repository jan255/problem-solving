#include <bits/stdc++.h>
using namespace std;

#define m2 (i << 1)

const int inf = 1e6;
const int N = 1e6 + 5;
int st[4 * N] = {0};


int dp[N];
int n;
vector<pair<int, int> > v;

void setMax(int x, int val, int i = 0, int l = 0, int r = N - 1) {
  if (x < l || x > r) return;
  if (l == r) {
    st[i] = val;
    return;
  }
  int mid = (l + r) >> 1;
  setMax(x, val, m2 + 1, l, mid);
  setMax(x, val, m2 + 2, mid + 1, r);
  st[i] = max(st[m2 + 1], st[m2 + 2]);
}

int getMax(int x, int y, int i = 0, int l = 0, int r = N - 1) {
  if (x > y) return 0;
  if (y < l || x > r) return -inf;
  if (x <= l && y >= r) return st[i];
  int mid = (l + r) >> 1;
  return max(getMax(x, y, m2 + 1, l, mid),
              getMax(x, y, m2 + 2, mid + 1, r));
}

int lis() {
  int l = 0;
  for (int i = 0; i < n; i++) {
    if (i == 0 || v[i].first != v[i - 1].first) {
      int j = i;
      while (j < n && v[j].first == v[i].first) {
        dp[j] = 1 + getMax(1, v[j].second - 1);
        j++;
      }
      j = i;
      while (j < n && v[j].first == v[i].first) {
        setMax(v[j].second, dp[j]);
        l = max(dp[j], l);
        cout << dp[j] << endl;
        j++;
      }
    }
  }
  return l;
}

int main() {
  cin >> n;

  int lis = 0;
  for (int i = 0; i < n; ++i) {
    int x; cin >> x;
    dp[x] = 1 + (x ? dp[x - 1] : 0);
    lis = max(lis, dp[x]);
  }
  cout << lis << endl;
  

  return 0;
}