#include <bits/stdc++.h>
using namespace std;


int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    vector<int> v(n);
    int ar[n];
    for (int i = 0; i < n; ++i) {
      cin >> v[i];
      ar[i] = i;
    }
    
    sort(ar, ar + n, [&v](const int& a, const int& b) {
      return v[a] < v[b];
    });

    int maxLen = 1;
    int dp[n] = {1};
    for (int i = 1; i < n; ++i) {
      dp[i] = 1;
      for (int j = 0; j < i; ++j) {
        if (v[ar[i]] - 1 == v[ar[j]]) {
          dp[i] = max(dp[i], 1 + dp[j]);
        }
      }
      maxLen = max(dp[i], maxLen);
    }
    cout << maxLen << endl;
  } 

  return 0;
}