#include <bits/stdc++.h>
using namespace std;

struct StreamMedian {

  priority_queue<int, vector<int>, less<int> > l;
  priority_queue<int, vector<int>, greater<int> > r;
  int sz;

  StreamMedian() : sz(0) { }


  void addx(int x) {
    if (sz == 0) {
      r.push(x);
    } else if (!l.empty() and x <= l.top()) {
      l.push(x);
    } else {
      r.push(x);
    }
    sz++;

    if (l.size() == r.size()) {
      return;
    } else if (l.size() > r.size()) {
      while (l.size() > r.size()) {
        r.push(l.top()); l.pop();
      }
    } else if (r.size() - sz % 2 > l.size()) {
      while (r.size() - sz % 2 > l.size()) {
        l.push(r.top()); r.pop();
      }
    }
  }

  int getMedian() {
    if (l.empty() and r.empty()) return -1;

    if (sz % 2 == 0) {
      return (l.top() + r.top()) / 2;
    } else {
      return r.top();
    }
  }

} stre;


int main() {
  int n; cin >> n;
  while (n--) {
    int x; cin >> x;
    stre.addx(x);
    cout << stre.getMedian() << endl;
  } 

  return 0;
}