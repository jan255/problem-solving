#include <bits/stdc++.h>
using namespace std;

const int N = 1e6;
char s[N];
int f[N];
int len;

void buildZ() {
  f[0] = 0;
  int l = 0, r = 0;
  for (int i = 1; i < len; ++i) {
    if (i > r) {
      l = r = i;
      while (r < len && s[r - l] == s[r]) r++;
      f[i] = r - l;
      r--;
    }
    else {
      int k = i - l;
      if (f[k] < r - i + 1) {
        f[i] = f[k];
      }
      else {
        l = i;
        while (r < len && s[r] == s[r - l]) r++;
        f[i] = r - l;
        r--;
      }
    }
  }
}

int main() {
  int T; cin >> T;
  while (T-- > 0) {
    scanf("%s", s);
    len = strlen(s);
    reverse(s, s + len);
    buildZ();
    reverse(f, f + len);
    f[len - 1] = len;
    int q; cin >> q;
    while (q--) {
      int x; 
      scanf("%d", &x);
      printf("%d\n", f[x - 1]);
    }
  }

  return 0;
}