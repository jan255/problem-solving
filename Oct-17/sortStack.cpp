#include <bits/stdc++.h>
using namespace std;

struct StackSorter {

  stack<int> st;

  StackSorter(int n = 10) {
    srand(time(NULL));
    for (int i = 0; i < n; ++i)
      st.push(rand() % n);
  }

  void sortStack() {
    if (st.empty()) return;
    sortRecursive();
  }

  void sortRecursive() {
    if (st.empty()) return;
    int t = st.top(); st.pop();
    sortRecursive();
    sortInto(t);
  }

  void sortInto(int t) {
    if (st.empty() or st.top() <= t) {
      st.push(t);
      return;
    }

    int tp = st.top(); st.pop();
    sortInto(t);
    st.push(tp);
  }

  void printStack() {
    while (!st.empty()) {
      cout << st.top() << endl;
      st.pop();
    }
  }

} sorter;


int main() {
  
  sorter.sortStack();
  sorter.printStack();

  return 0;
}