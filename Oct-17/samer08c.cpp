#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
vector<int> candies[N];
vector<pair<int, int> > dp[N];
vector<int> rowMax;
int n, m;

int DP[N];

int main() {
  while (1) {
    cin >> n >> m;
    if (!n) break;
    for (int i = 0; i < n; ++i) {
      candies[i].resize(m);
      dp[i].resize(m);
    }

    for (int i = 0; i < n; ++i)
      for (int j = 0; j < m; ++j)
        cin >> candies[i][j];

    for (int i = 0; i < n; ++i) {
      dp[i][0].first = candies[i][0];
      for (int j = 1; j < m; ++j) {
        dp[i][j].first = max(dp[i][j - 1].first,
          candies[i][j] + (j > 1 ? dp[i][j - 2].first : 0));
      }
    }

    for (int i = 0; i < n; ++i) {
      dp[i][m - 1].second = candies[i][m - 1];
      for (int j = m - 2; j >= 0; --j) {
        dp[i][j].second = max(dp[i][j + 1].second,
          candies[i][j] + (j < m - 2 ? dp[i][j + 2].second : 0));
      }
    }

    rowMax.clear();
    for (int i = 0; i < n; ++i) {
      rowMax.push_back(0);
      for (int j = 0; j < m; ++j) {
        rowMax[i] = max(rowMax[i], candies[i][j] +
          (j > 1 ? dp[i][j - 2].first : 0) + (j < m - 2 ? dp[i][j + 2].second : 0));
      }
    }

    int a = 0, b = 0;
    for (int i = 0; i < n; ++i) {
      int ab = max(rowMax[i] + a, b);
      a = b;
      b = ab;
    }

    cout << b << endl;
  } 

  return 0;
}