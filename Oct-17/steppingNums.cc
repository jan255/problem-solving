void pushNextNums(unordered_set<int>& seenNums, int num, queue<int>& q) {
  if (seenNums.count(num) == 0) {
    q.push(num);
    seenNums.insert(num);
  }
}


void pushNextNums(num, queue<int>& q, unordered_set<int>& seenNums, int B) {
  int lastDig = num % 10;
  num *= 10;
  if (lastDig < 9) {
    int newNum = num + (lastDig + 1);
    pushIfnotSeen(seenNums, newNum, q);
  }
  if (lastDig > 0) {
    int newNum = num + (lastDig - 1);
    pushIfnotSeen(seenNums, newNum, q);
  }
}

vector<int> Solution::stepnum(int A, int B) {
  unordered_set<int> seenNums;

  queue<int> q;
  for (int i = 0; i < 10; ++i) {
    q.push(i);
    seenNums.insert(i);
  }

  vector<int> stepNums;
  while (!q.empty()) {
    int num = q.top();
    q.pop();
    if (num >= A) {
      stepNums.push_back(num);
    }
    pushNextNums(num, q, seenNums, B);
  }

  return stepNums;
}
