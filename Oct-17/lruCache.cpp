#include <bits/stdc++.h>
using namespace std;

struct Node {
  int data;
  Node* prev;
  Node* next;
};

struct LruCache {

  unordered_map<int, Node*> hasher;
  Node* dll;
  int sz; // size

  LruCache(int sz) : sz(sz) { }

  void access(int x) {
    if (hasher.count(x) == 0) {
      Node* node = new Node();
      node.data = x;
      node.prev = NULL;
      node.next = dll;
      dll = node;
      sz++;
    } else {
      Node* node = hasher[x];
      removeNode(node);
      addNode(node);
    }
  }

  void invalidateLru() {

  }

};



int main() {
  

  return 0;
}