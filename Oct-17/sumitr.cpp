#include <bits/stdc++.h>
using namespace std;


int tri[100][100];

int main() {
  int n; cin >> n;
  while (n--) {
    int h; cin >> h;
    for (int i = 0; i < h; ++i) {
      for (int j = 0; j <= i; ++j) {
        cin >> tri[i][j];
      }
    }
    for (int i = h - 1; i >= 0; --i) {
      for (int j = 0; j <= i; ++j) {
        tri[i][j] += max(tri[i + 1][j], tri[i + 1][j + 1]);
      }
    }

    cout << tri[0][0] << endl;
  } 

  return 0;
}