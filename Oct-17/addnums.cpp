#include <bits/stdc++.h>
using namespace std;

string a[n];
string b[m];

void add() {
  reverse(a, a + n);
  reverse(b, b + n);

  int carry = 0;
  for (int i = 0; i < min(n, m); ++i) {
    int s = carry + a[i] + b[i];
    a[i] = s % 10;
    carry = s / 10;
  }
  for (int i = min(n, m); i < max(n, m); ++i) {
    if (n < m) {
      a.push_back(b[i]);
    }
    carry += a[i];
    a[i] = carry % 10;
    carry /= 10;
  }

  reverse(a, a + max(n, m));
}

int main() {
  

  return 0;
}