#include <bits/stdc++.h>
#include <unordered_map>
using namespace std;
const int N = 1e6 + 2;
int ar[N];
int bit[N];

void add(int c) {
  if (c == 0) return;
  while (c < N) {
    bit[c]++;
    c += c & -c;
  }
}

int get(int c) {
  int s = 0;
  while (c > 0) {
    s += bit[c];
    c -= c & -c;
  }
  return s;
}

int main() {
  int n; cin >> n;
  unordered_map<int, int> maper;
  int c = 1;
  unordered_map<int, int> counts;
  for (int i = 0; i < n; ++i) {
    int x; scanf("%d", &x);
    if (maper.find(x) == maper.end()) {
      maper[x] = c++;
    }
    ar[i] = maper[x];
    counts[ar[i]]++;
  }

  unordered_map<int, int> counts2;
  long long ans = 0;
  for (int i = n - 2; i > 0; --i) {
    counts[ar[i + 1]]--;
    counts2[ar[i + 1]]++;
    add(counts2[ar[i + 1]]);

    int fi = counts[ar[i]];
    ans += get(counts[ar[i]] - 1);
  }
  cout << ans << endl;

  return 0;
}