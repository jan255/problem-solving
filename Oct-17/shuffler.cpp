#include <bits/stdc++.h>
using namespace std;

const int n = 10;
int ar[n];

void shuffle() {
  for (int i = 1; i < n; ++i) {
    int j = (rand() % (n - i)) + i;
    swap(ar[i - 1], ar[j]);
    cout << ar[i - 1] << " ";
  }
  cout << ar[n - 1] << endl;
}

int main() {
  srand(time(NULL));

  for (int i = 0; i < n; ++i) {
    ar[i] = rand() % n;
    cout << ar[i] << " ";
  }
  cout << endl;

  shuffle();

  return 0;
}