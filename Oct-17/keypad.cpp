#include <bits/stdc++.h>
using namespace std;

const int n = 1e6;
int a[n];
int b[n];

int find(int bval) {
  int l = 0, r = n - 1;
  while (l < r) {
    int mid = (l + r) >> 1;
    if (b[mid] < bval) l = mid + 1;
    else r = mid;
  }
  return bval == b[l] ? l + 1 : 0;
}

void solve() {
  int asum = 0, bsum = 0;
  for (int i = 0; i < n; ++i) {
    asum += a[i];
    bsum += b[i];
  }
  sort(a, a + n);
  sort(b, b + n);

  int diff = asum - bsum;
  for (int i = 0; i < n; ++i) {
    int j = 0;
    if (j = find(a[i] + diff)) {
      cout << a[i] << " " << b[j - 1] << endl;
      return;
    }
  }
}

int main() {

  return 0;
}