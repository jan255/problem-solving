#include <map>
#include <set>
#include <list>
#include <cmath>
#include <ctime>
#include <deque>
#include <queue>
#include <stack>
#include <string>
#include <bitset>
#include <cstdio>
#include <limits>
#include <vector>
#include <climits>
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <numeric>
#include <sstream>
#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <unordered_set>
using namespace std;

const int CABS = (1 << 10) + 1;
vector<int> sizes;

bool err = false;

void read_sizes() {
  string line; getline(cin, line);
  istringstream iss(line);
  string sz;
  while (getline(iss, sz, ' ')) {
    int cabSize = stoi(sz);
    sizes.push_back(cabSize);
    err = err || (cabSize <= 0 || cabSize >= (1 << 10));
  }
}

map<long long, long long> timeToKey[CABS];
map<long long, long long> keyToTime[CABS];
map<long long, int> itemToCab;

void checkOverflows() {
  int cabNum = 0;
  long long itemNum;
  long long lastTime;
  while (cabNum < sizes.size() &&
    keyToTime[cabNum].size() > sizes[cabNum]) {

    map<long long, long long>::iterator it = timeToKey[cabNum].begin();
    itemNum = it->second;
    lastTime = it->first;

    timeToKey[cabNum].erase(lastTime);
    keyToTime[cabNum].erase(itemNum);
    itemToCab.erase(itemNum);

    timeToKey[cabNum + 1][lastTime] = itemNum;
    keyToTime[cabNum + 1][itemNum] = lastTime;
    itemToCab[itemNum] = cabNum + 1;

    ++cabNum;
  }

  if (cabNum == sizes.size()) {
    itemToCab[itemNum] = -1;
    timeToKey[sizes.size()].clear();
    timeToKey[sizes.size()].clear();
  }
}

void putBack(long long itemNum, long long time) {
  itemToCab[itemNum] = 0;
  keyToTime[0][itemNum] = time;
  timeToKey[0][time] = itemNum;

  checkOverflows();
}

string process(long long itemNum, long long time) {
  string status = "NEW";
  if (itemToCab.find(itemNum) == itemToCab.end()) {
    // new
    putBack(itemNum, time);
  } else {
    // already done
    int cabNum = itemToCab[itemNum];
    if (cabNum != -1) { // inside
      status = to_string(cabNum + 1);
      long long lastTime = keyToTime[cabNum][itemNum];
      timeToKey[cabNum].erase(lastTime);
      keyToTime[cabNum].erase(itemNum);
    } else {
      status = "OUTSIDE";
    }

    itemToCab[itemNum] = -1; // taken out
    putBack(itemNum, time);
  }

  return status;
}

int main() {
  read_sizes();
  err = err || (sizes.size() <= 0 || sizes.size() >= (1 << 6));

  long long k; cin >> k;
  err = err || (k <= 0 || k >= (1LL << 32));
  long long time = 0;
  while (k-- > 1) {
    long long itemNum; cin >> itemNum;
    err = err || (itemNum <= 0 || itemNum >= (1LL << 32));
    process(itemNum, time++);
  }

  long long lastItem; cin >> lastItem;
  err = err || (lastItem <= 0 || lastItem >= (1LL << 32));
  string lastStatus = process(lastItem, time);

  cout << (err ? "INPUT_ERROR" : lastStatus) << endl;
  
  return 0;
}