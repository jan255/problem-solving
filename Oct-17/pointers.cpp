#include <bits/stdc++.h>
using namespace std;

class EqTest {

public:
  int data;

  EqTest(int data) : data(data) { }

};

int perform(int a, int b, int (*operation)(int, int)) {
  return (*operation)(a, b);
}

int add(int a, int b) {
  return a + b;
}

int sub(int a, int b) {
  return a - b;
}

int main() {


  int (*funcPtr)(int, int) = add;

  cout << perform(23, 24, funcPtr) << endl;


  return 0;
}