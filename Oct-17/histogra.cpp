#include <bits/stdc++.h>
using namespace std;


int main() {
  while (1) {
    int n; cin >> n;
    if (!n) break;

    stack<int> stk;
    vector<int> v;
    
    long long maxArea = 0;
    for (int i = 0; i < n; ++i) {
      int x; cin >> x;
      v.push_back(x);
      if (stk.empty() or v[stk.top()] < x) {
        stk.push(i);
      } else {
        while (!stk.empty() and v[stk.top()] > x) {
          int tp = stk.top(); stk.pop();
          int leftIdx = stk.empty() ? -1 : stk.top();
          maxArea = max(maxArea, 1LL * x * (tp - leftIdx));
        }

        if (stk.empty() || v[stk.top()] < x) stk.push(i);
      }
    }

    while (!stk.empty()) {
      int tp = stk.top(); stk.pop();
      int leftIdx = stk.empty() ? -1 : stk.top();
      maxArea = max(maxArea, 1LL * v[tp] * (tp - leftIdx));
    }

    cout << maxArea << endl;
  } 

  return 0;
}