#include <bits/stdc++.h>
using namespace std;

void computeLeastPrimes(vector<int>& leastPrimeFact) {
  unordered_set<int> nonprimes;
  for (int i = 2; i < (int)leastPrimeFact.size(); ++i) {
    if (nonprimes.count(i) == 0) {
      leastPrimeFact[i] = i;
      int j = i + i;
      while (j < (int)leastPrimeFact.size()) {
        if (leastPrimeFact[j] == 0) {
          leastPrimeFact[j] = i;
          nonprimes.insert(j);
        }
        j += i;
      }
    }
  }
}

int numFacts(vector<int>& leastPrimeFact, int n) {
  int numFacts = 0;
  while (n > 1) {
    n = n / leastPrimeFact[n];
  }
  return numFacts;
}

vector<int> solution(int N, vector<int> &P, vector<int> &Q) {
  vector<int> leastPrimeFact(N + 1);
  computeLeastPrimes(leastPrimeFact);
  vector<int> numsemis(N + 1);
  for (int i = 4; i <= N; ++i) {
    numsemis[i] += numsemis[i - 1] + (numFacts(leastPrimeFact, i) == 2);
  }

  vector<int> counts;
  for (int i = 0; i < (int)P.size(); ++i) {
    counts.push_back(numsemis[Q[i]] - numsemis[P[i] - 1]);
  }
  return counts;
}




int main() {
  

  return 0;
}