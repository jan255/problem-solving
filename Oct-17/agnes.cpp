#include <bits/stdc++.h>
using namespace std;

vector<int> agnes;
vector<int> route;

const int N = 2001;
int DP[N][N];

int dp(int ai, int ri) {
  if (ai < 0 || ri < 0) return 0;
  if (DP[ai][ri] != -1) return DP[ai][ri];
  DP[ai][ri] = max(dp(ai - 1, ri), dp(ai, ri - 1));
  if (agnes[ai] == route[ri])
    DP[ai][ri] = max(DP[ai][ri], 1 + dp(ai - 1, ri - 1));
  return DP[ai][ri];
}


int main() {
  int sets; cin >> sets;
  while (sets--) {
    
    agnes.clear();
    int x; cin >> x; 
    while (x != 0) {
      agnes.push_back(x);
      cin >> x;
    }

    int dates = 0;
    while (1) {
      int x; cin >> x;
      if (x == 0) {
        cout << dates << endl;
        break;
      }
      route.clear();
      while (x != 0) {
        route.push_back(x);
        cin >> x;
      }

      memset(DP, -1, sizeof(int) * N * N);
      dates = max(dates, dp(agnes.size() - 1, route.size() - 1));
    }
  } 

  return 0;
}