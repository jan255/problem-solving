#include <bits/stdc++.h>
using namespace std;

const int N = 5001;
int DP[N][N];
int n;
string s;

int dp(int a, int b) {
  if (a >= b) return 0;
  if (DP[a][b] != -1) return DP[a][b];
  DP[a][b] = 1 + min(dp(a + 1, b), dp(a, b - 1));
  if (s[a] == s[b]) DP[a][b] = min(DP[a][b], dp(a + 1, b - 1));
  return DP[a][b];
}

int main() {
  cin >> n;
  cin >> s;

  memset(DP, -1, sizeof(int) * N * N);

  cout << dp(0, n - 1) << endl;

  return 0;
}