#include <bits/stdc++.h>
using namespace std;


int main() {
  int m, s;
  cin >> m >> s;

  if ((m > 1 && s == 0) || m * 9 < s) {
    cout << "-1 -1";
    return 0;
  }

  // min
  string mn;
  mn.resize(m);
  for (int i = 0; i < m; ++i)
    mn[i] = '0';
  mn[0] = '1';
  for (int i = m - 1, tmp = s - 1; i >= 0; --i) {
    mn[i] += min(9, tmp);
    tmp -= min(9, tmp);
  }

  //max;
  string mx;
  mx.resize(m);
  for (int i = 0; i < m; ++i)
    mx[i] = '0';
  for (int i = 0, tmp = s; i < m; ++i) {
    mx[i] += min(9, tmp);
    tmp -= min(9, tmp);
  }

  cout << mn << " " << mx << endl;

  return 0;
}