#include <bits/stdc++.h>
using namespace std;

const int N = 56;
int DP[N][N];

int dp(int n, int k) {
  if (k <= 0) return 0;
  if (n <= 0) return N;
  if (DP[n][k] != -1) return DP[n][k];
  int sol = N;
  for (int i = 1; i <= k; ++i) {
    sol = min(sol, 1 + max(dp(n - 1, i - 1), dp(n, k - i)));
  }
  return DP[n][k] = sol;
}

int main() {
  int t; cin >> t;
  while (t-- > 0) {
    int n; cin >> n;
    int k; cin >> k;
    memset(DP, -1, N * N * sizeof(int));
    cout << dp(n, k) << endl;
  } 

  return 0;
}