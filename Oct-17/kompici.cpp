#include <bits/stdc++.h>
using namespace std;

const int N = 5e5 + 1;
int msk[N];

int main() {
  int n; cin >> n;
  for (int i = 0; i < n; ++i) {
    long long x;
    scanf("%lld", &x);
    while (x) {
      msk[i] |= (1 << (x % 10));
      x /= 10;
    }
  }

  int counts[(1 << 10)] = {0};
  long long numPairs = 0;
  for (int i = 0; i < n; ++i) {
    long long nots = 0;
    for (int j = 1; j < (1 << 10); ++j) {
      if (!(j & msk[i])) {
        nots += counts[j];
      }
    }
    numPairs += (i - nots);
    counts[msk[i]]++;
  }
  cout << numPairs << endl;  

  return 0;
}