#include <bits/stdc++.h>
using namespace std;



int Solution::solve(string A) {
  int n = A.length()
  vector<char> chars;
  for (auto c : A) chars.push_back(c);
  chars.push_back('%');
  for (int i = n - 1; i >= 0; --i)
    chars.push_back(chars[i]);

  int j = 0;
  for (int i = n + 1; i < chars.size(); ++i) {
    if (chars[i] != chars[j]) j = 0;
    else ++j;
  }

  return n - j;
}



int main() {
   

  return 0;
}