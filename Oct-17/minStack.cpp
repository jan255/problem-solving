#include <bits/stdc++.h>
using namespace std;

struct Stack {

  stack<int> st;
  int minElem;

  void add(int x) {
    if (st.empty()) {
      st.push(x);
      minElem = x;
    } else if (x < minElem) {
      st.push(2 * x - minElem);
      minElem = x;
    } else {
      st.push(x);
    }
  }

  int get() {
    if (st.empty()) {
      return -1;
    } else if (st.top() >= minElem) {
      int x = st.top(); st.pop();
      return x;
    } else {
      int minPrev = 2 * minElem - st.top();
      int x = minElem; st.pop();
      minElem = minPrev;
      return x;
    }
    return -1;
  }

  int getMin() {
    return minElem;
  }

  void clearStack() {
    while (!st.empty()) st.pop();
    minElem = -1;
  }

} st;


int main() {
  int t; cin >> t;
  while (t--) {
    st.clearStack();

    int q; cin >> q;
    while (q--) {
      int op; cin >> op;
      if (op == 1) {
        int x; cin >> x;
        st.add(x);
      } else if (op == 2) {
        cout << st.get() << endl;
      } else if (op == 3) {
        cout << st.getMin() << endl;
      }
    }
  }

  return 0;
}