#include <bits/stdc++.h>
using namespace std;

void computeRectEndingWith(vector<long long>& v, vector<long long>& sols) {
  stack<long long> stk;
  for (long long i = 0; i < v.size(); ++i) {
    if (stk.empty()) {
      stk.push(i);
      sols[i] = v[i];
    } else {
      while (!stk.empty() and v[stk.top()] >= v[i])
        stk.pop();
      long long leftIdx = stk.empty() ? -1 : stk.top();
      sols[i] = (i - leftIdx) * v[i];
      stk.push(i);
    }
  }
}

int main() {
  while (1) {
    long long n; cin >> n;
    if (!n) break;

    vector<long long> v(n);
    for (long long i = 0; i < n; ++i)
      cin >> v[i];

    vector<long long> prefixSols(n);
    computeRectEndingWith(v, prefixSols);

    reverse(v.begin(), v.end());
    vector<long long> suffixSols(n);
    computeRectEndingWith(v, suffixSols);
    reverse(suffixSols.begin(), suffixSols.end());

    reverse(v.begin(), v.end());
    long long maxArea = 0;
    for (long long i = 0; i < n; ++i)
      maxArea = max(maxArea, prefixSols[i] + suffixSols[i] - v[i]);
    cout << maxArea << endl;
  } 

  return 0;
}