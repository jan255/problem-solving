#include <bits/stdc++.h>
using namespace std;

int dp[40];

int main() {
  dp[0] = dp[1] = 1;
  for (int i = 2; i < 26; ++i) {
    dp[i] = dp[i - 1] + dp[i - 2];
    cout << dp[i] << endl;
  }

  return 0;
}