#include <bits/stdc++.h>
using namespace std;


int a, b;
int DP[20][2][20][180];

int dp(int ai, int eq, int cnt, int sum, vector<int>& a, int dig) {
  if (ai >= a.size()) return cnt;
  if (DP[ai][eq][cnt][sum] != -1) return DP[ai][eq][cnt][sum];
  int sol = 0;
  if (eq) {
    for (int i = 0; i <= a[ai]; ++i)
      sol += dp(ai + 1, eq && (i == a[ai]), cnt + (sum + i > 0) * (i == dig), sum + i, a, dig);
  } else {
    for (int i = 0; i < 10; ++i)
      sol += dp(ai + 1, eq, cnt + (sum + i > 0) * (i == dig), sum + i, a, dig);
  }
  return DP[ai][eq][cnt][sum] = sol;
}

vector<int> counts(int a) {
  if (a < 0) {
    vector<int> v(10, 0);
    return v;
  }

  vector<int> digs;
  while (a > 0) {
    digs.push_back(a % 10);
    a /= 10;
  }
  reverse(digs.begin(), digs.end());

  vector<int> numDigs;
  for (int i = 0; i < 10; ++i) {
    memset(DP, -1, sizeof(int) * 20000);
    numDigs.push_back(dp(0, 1, 0, 0, digs, i));
  }
  numDigs[0] += 1;
  return numDigs;
}

int main() {
  while (1) {
    cin >> a >> b;
    if (!b) break;

    if (a > b) swap(a, b);

    vector<int> bv = counts(b);
    vector<int> av = counts(a - 1);
    for (int i = 0; i < 10; ++i)
      cout << (bv[i] - av[i]) << " ";
      // cout << bv[i] << "," << av[i] << " ";
    cout << endl;
  } 

  return 0;
}