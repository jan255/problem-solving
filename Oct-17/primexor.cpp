#include <bits/stdc++.h>
using namespace std;

const long long MOD = 1e9 + 7;
const int N = 1e5 + 1;
vector<int> primes;
int n;
vector<int> nums;
map<int, int> counts;
long long DP[1001][1 << 13];
long long p2[4501];

long long dp(int i, int p) {
  if (i < 0) return p == 0;
  if (DP[i][p] != -1) return DP[i][p];
  int c = counts[nums[i]];
  DP[i][p] = (dp(i - 1, p) * ) % MOD;

  return DP[i][p];
}

void solve() {
  p2[0] = 1;
  for (int i = 1; i < 4501; ++i) {
    p2[i] = (2 * p2[i - 1]) % MOD;
  }

  memset(DP, 0, sizeof(long long) * 1001 * 1LL * (1 << 13));
  for (int i = 0; i < nums.size(); ++i)
    for (int j = 0; j < (1 << 13); ++j)
      dp(i, j);

  long long ans = 0;
  for (int i = 0; i < primes.size(); ++i) {
    ans += dp(nums.size() - 1, primes[i]);
    ans %= MOD;
  }

  cout << ans << endl;
}

int main() {
  for (int i = 2; i < 4501; ++i) {
    if (!counts[i]) {
      primes.push_back(i);
      for (int j = i << 1; j < 4501; j += i)
        counts[j] = 1;
    }
  }

  int q; cin >> q;
  while (q--) {
    cin >> n;
    counts.clear();
    nums.clear();
    for (int i = 0; i < n; ++i) {
      cin >> x;
      if (!counts[x]) nums.push_back(x);
      else ++counts[x];
    }

    solve();
  } 

  return 0;
}