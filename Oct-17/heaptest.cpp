#include <bits/stdc++.h>
using namespace std;

priority_queue<int, vector<int>, less<int> > pq;


int main() {

  for (int i = 0; i < 10; ++i) {
    pq.push(rand() % 1000);
  }

  while (!pq.empty()) {
    cout << pq.top() << endl;
    pq.pop();
  }
  

  return 0;
}