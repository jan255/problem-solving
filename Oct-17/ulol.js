function dfs(root) {
    var depth = 0;
    var childNodes = root.childNodes;
    for (var i = 0; i < childNodes.length; ++i) {
        var add = (childNodes[i].tagName === "OL" || 
                        childNodes[i].tagName === "UL" ? 1 : 0);
        
        depth = Math.max(depth, add + dfs(childNodes[i]));
    }
    return depth;
}

function solution() {
    return dfs(document.getElementsByTagName('body')[0]);
}