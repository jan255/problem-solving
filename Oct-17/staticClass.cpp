#include <bits/stdc++.h>
using namespace std;

class StaticTest {
  static const double add = 0.5;

  public:
    static int roundDouble(double d) {
      return (int)(d + add);
    }

};


int main() {
  
  while (1) {
    double d; cin >> d;
    if (!d) break;

    cout << StaticTest::roundDouble(d) << endl;
  }


  return 0;
}