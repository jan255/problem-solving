#include <bits/stdc++.h>
using namespace std;

const int n = 1e5;
int psum[n][2];

int findEq(int i) {
  int j = i + 1;
  while (j < n) {
    if (psum[j][0] - psum[i - 1][0] == psum[j][1] - psum[i - 1][1]) {
      return j;
    }
    int diff = abs(psum[j][0] - psum[i - 1][0] - psum[j][1] + psum[i - 1][1]);
    j += diff;
  }
  return -1;
}

int main() {

  srand(time(NULL));
  for (int i = 0; i < n; ++i) {
    int x = rand() % 2;
    psum[i][x]++;
    if (i) {
      psum[i][x] += psum[i - 1][x];
      psum[i][!x] += psum[i - 1][!x];
    }
  }

  for (int i = 0; i < n; ++i) {
    int x = findEq(i);
    if (x != -1) {
      cout << i << " " << x << endl;
    }
  }
  

  return 0;
}