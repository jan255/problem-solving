// decimal to binary
#include <iostream>
using namespace std;

const int LEN = 32;

void decToBin(double d) {

  cout << ".";
  for (int i = 0; i < LEN; ++i) {
    d -= (int)d;
    if (d == 0) break;
    d = d * 2;
    cout << (int)d;
  }
  cout << endl;
}

int main() {

  decToBin(0.72);

  return 0;
}