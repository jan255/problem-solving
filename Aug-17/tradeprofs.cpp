#include <bits/stdc++.h>
using namespace std;

const int N = 30;
int DP[N][N];
int k, n;
int p[N];
int inf = -1e8;

int dp(int ai, int ki) {
  if (ki == 0) return 0;
  if (ai >= n - 1) {
    return 0;
  }
  if (DP[ai][ki] != -1) return DP[ai][ki];
  DP[ai][ki] = dp(ai + 1, ki);
  int buy = p[ai];
  for (int j = ai + 1; j < n; ++j) {
    if (p[j] > buy) 
      DP[ai][ki] = max(DP[ai][ki], p[j] -buy + dp(j + 1, ki - 1));
  }
  return DP[ai][ki];
}

int main() {
  int q; cin >> q;
  while (q--) {
    cin >> k >> n;
    for (int i = 0; i < n; ++i)
      cin >> p[i];
    for (int i = 0; i < N; ++i)
      for (int j = 0; j < N; ++j)
        DP[i][j] = -1;

    cout << dp(0, k) << endl;
  } 

  return 0;
}