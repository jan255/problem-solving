#include <bits/stdc++.h>
using namespace std;

int ar[123];

int main() {
  int t; cin >> t;
  while (t--) {
    int n; cin >> n;
    set<int> st;
    for (int i = 0; i < n; ++i) {
      cin >> ar[i];
      st.insert(ar[i]);
    }

    string rainb = st.size() == 7 and ar[0] == 1 and ar[n - 1] == 1 ? "yes" : "no";
    for (int i = 0, j = n - 1; i <= j; ++i, --j) {
      if (ar[i] > 7 || (i && (ar[i] < ar[i - 1] || ar[i] - ar[i - 1] > 1)) || ar[i] != ar[j]) {
        rainb = "no";
        break;
      }
    } 
    cout << rainb << endl;
  } 

  return 0;
}