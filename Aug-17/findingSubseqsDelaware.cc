// https://www.hackerrank.com/contests/openbracket-2017/challenges/finding-subsequence
// SPENT OVER 1 DAY ON THIS ONE. GOT IT SO WRONG ALL THE WHILE.
#include <bits/stdc++.h>
using namespace std;

void solve(string s, int k) {
  int arr[26] = {0};
  int lasti[26] = {0};
  for (int i = 0; i < s.length(); ++i) {
    arr[s[i] - 97]++;
    lasti[s[i] - 97] = i + 1;
  }
  int res[26] = {0};
  vector<char> t;
  for (int i = 25, j = 0; i >= 0; --i) {
    if (arr[i] < k) {
      continue;
    }
    int tmpj = j;
    while (arr[i] > 0 && j < s.length()) {
      if (s[j] != i + 97) {
        j++;
      } else {
        arr[i]--; j++;
        res[i]++;
      }
    }
    if (res[i] >= k) {
      while (res[i]--) t.push_back(i + 97);
      j = lasti[i];
    } else {
      j = tmpj;
    }
  }
  for (int i = 0; i < t.size(); ++i) {
    cout << t[i];
  }
}

int main() {
  string s; cin >> s;
  int k; cin >> k;
  solve(s, k);
  return 0;
}
