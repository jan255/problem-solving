#include <bits/stdc++.h>
using namespace std;


int main() {
  int q; cin >> q;
  int p[100];
  while (q--) {
    int n, k, x, d;
    cin >> n >> k >> x >> d;
    double one = 0;
    for (int i = 0; i < n; ++i) {
      cin >> p[i];
      one += max(k * 1.0, (p[i] / (100.0)) * x);
    }
    cout << (one > d ? "upfront" : "fee") << endl;
  } 

  return 0;
}