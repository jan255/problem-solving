// https://www.codechef.com/AUG17/problems/CHEFMOVR

#include <bits/stdc++.h>
using namespace std;

const int N = 1e5;
int ar[N];
bool vis[N];

int main() {
  int t; cin >> t;
  while (t--) {
    int n, d;
    cin >> n >> d;
    long long sum = 0;
    for (int i = 0; i < n; ++i) {
      cin >> ar[i];
      sum += ar[i];
      vis[i] = false;
    }

    if (sum % n) {
      cout << -1 << endl;
      continue;
    }

    long long moves = 0;
    for (int i = 0; i < n; ++i) {
      if (vis[i]) continue;

      long long lsum = 0;
      int ln = 0;
      for (int j = i; j < n; j += d) {
        ln++;
        lsum += ar[j];
        vis[j] = true;
      }

      if (lsum % ln > 0 || sum / n != lsum / ln) {
        moves = -1;
        break;
      }

      long long lavg = lsum / ln;
      queue<int> lt;
      queue<int> gt;
      for (int j = i; j < n; j += d) {
        if (ar[j] < lavg) lt.push(j);
        else if (ar[j] > lavg) gt.push(j);  
        ar[j] = abs(ar[j] - lavg);
      }

      while (!gt.empty()) {
        int f = gt.front(); gt.pop();
        while (ar[f]) {
          if (ar[lt.front()] <= ar[f]) {
            moves += ar[lt.front()] * 1LL * (abs(lt.front() - f) / d);
            ar[f] -= ar[lt.front()];
            ar[lt.front()] = 0;
          } else {
            moves += ar[f] * 1LL * (abs(lt.front() - f) / d);
            ar[lt.front()] -= ar[f];
            ar[f] = 0;
          }
          if (!ar[lt.front()]) lt.pop();
        }
      }

    }

    cout << moves << endl;
  }  

  return 0;
}