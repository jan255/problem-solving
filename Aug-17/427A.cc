#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
const int MOD = 1e9 + 7;

struct Solver {
  vector<int> g[N];
  vector<int> rg[N];
  int n, m;
  int cost[N];
  stack<int> st;
  bool vis[N];
  int scc[N];
  int c;
  int sccCost[N][2];
  
  void solve() {
    for (int i = 0; i < N; ++i)
      sccCost[i][0] = MOD;
    for (int i = 1; i <= n; ++i) {
      int c = cost[i];
      int sccNum = scc[i];
      if (sccCost[sccNum][0] > c) {
        sccCost[sccNum][0] = c;
        sccCost[sccNum][1] = 1;
      } else if (sccCost[sccNum][0] == c) {
        sccCost[sccNum][1]++;
      }
    }
    long long total = 0;
    long long ways = 1;
    for (int sccNum = 1; sccNum < c; ++sccNum) {
      total += sccCost[sccNum][0];
      ways = (ways * 1LL * sccCost[sccNum][1]) % MOD;
    }
    cout << total << " " << ways << endl;
  }

  void assignScc(int v, int c) {
    if (scc[v]) return;
    scc[v] = c;
    for (int i = 0; i < rg[v].size(); ++i)
      assignScc(rg[v][i], c);
  }

  void fillScc() {
    c = 1;
    while (!st.empty()) {
      int v = st.top(); st.pop();
      if (scc[v]) continue;
      assignScc(v, c++);
    }
  }

  void dfsRec(int v) {
    if (vis[v]) return;
    vis[v] = true;
    for (int i = 0; i < this->g[v].size(); ++i)
      dfsRec(this->g[v][i]);
    this->st.push(v);
  }

  void dfs() {
    for (int i = 1; i <= n; ++i)
      dfsRec(i);
  }

  void input() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
      cin >> this->cost[i];
      this->vis[i] = false;
      this->scc[i] = 0;
    }
    cin >> m;
    while (m--) {
      int u, v;
      cin >> u >> v;
      this->g[u].push_back(v);
      this->rg[v].push_back(u);
    }
  }

};

int main() {
  Solver s;
  s.input();
  s.dfs();
  s.fillScc();
  s.solve();

  return 0;
}