// https://www.hackerrank.com/contests/101hack51/challenges/charging-the-batteries
// WARNINGS: com.problemsolving.chargingbatterries: Code duplications. Please fix this

#include <bits/stdc++.h>
using namespace std;

int n, m, k;
map<pair<int, int>, int> mp;
vector<pair<int, int> > v;

bool comp(const pair<int, int>& a, const pair<int, int>& b) {
  return mp[a] < mp[b];
}

int main() {
  cin >> n >> m >> k;
  if (!k) {
    cout << 0;
    return 0;
  }

  for (int i = 0; i < m; ++i) {
    int x, y;
    cin >> x >> y;
    v.push_back(make_pair(x, y));
    mp[make_pair(x, y)] = 0;
  }

  int x = 0;
  int y = 0;
  int d = 0;
  for (x = 0; x <= n; ++x) {
    if (mp.find(make_pair(x, y)) != mp.end()) {
      mp[make_pair(x, y)] = d;
    }
    d++;
  }
  x--;
  for (y = 1; y <= n; ++y) {
    if (mp.find(make_pair(x, y)) != mp.end()) {
      mp[make_pair(x, y)] = d;
    }
    d++;
  }
  --y;
  for (--x ; x >= 0; --x) {
    if (mp.find(make_pair(x, y)) != mp.end()) {
      mp[make_pair(x, y)] = d;
    }
    d++;
  }
  ++x;
  for (--y ; y > 0; --y) {
    if (mp.find(make_pair(x, y)) != mp.end()) {
      mp[make_pair(x, y)] = d;
    }
    d++;
  }

  sort(v.begin(), v.end(), comp);

  vector<long long> ds;
  for (int i = 1; i < m; ++i) {
    int dx = mp[v[i]] - mp[v[i - 1]];
    ds.push_back(dx);
  }
  ds.push_back(4 * n - mp[v[m - 1]] + mp[v[0]]);
  int N = ds.size();

  for (int i = 0; i < k; ++i)
    ds.push_back(ds[i]);

  for (int i = 1; i < ds.size(); ++i) {
    ds[i] += ds[i - 1];
  }

  long long res = 1e12;
  for (int i = 0; i < N; ++i) {
    int r = i + k - 2;
    long long dist = ds[r] - (i ? ds[i - 1] : 0);
    res = min(res, dist);
  }
  cout << res << endl;


  return 0;
}