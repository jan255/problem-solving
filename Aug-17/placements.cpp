#include <bits/stdc++.h>
using namespace std;

const int N = 1234;
string res[N];
long long minSal[N];
long long maxOff[N];
int maxPos[N];
int got[N];

int main() {
  int t; cin >> t;
  while (t--) {
    int n, m;
    cin >> n >> m;
    for (int i = 0; i < n; ++i)
      cin >> minSal[i];
    for (int i = 0; i < m; ++i) {
      cin >> maxOff[i] >> maxPos[i];
      got[i] = 0;
    }
    int numCands = 0;
    long long sal = 0;
    for (int i = 0; i < n; ++i) {
      cin >> res[i];
      int from = -1;
      for (int j = 0; j < m; ++j) {
        if (res[i][j] == '1' && got[j] < maxPos[j] && maxOff[j] >= minSal[i]) {
          if (from < 0 || maxOff[j] > maxOff[from]) from = j;
        }
      }
      if (from >= 0) {
        numCands++;
        sal += maxOff[from];
        got[from]++;
      }
    }
    int comps = 0;
    for (int i = 0; i < m; ++i) {
      comps += got[i] == 0;
    }
    cout << numCands << " " << sal << " " << comps << endl;
  } 

  return 0;
}