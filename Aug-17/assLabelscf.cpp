#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int n, m;
vector<int> g[N];
set<int> h[N];
bool vis[N];
int perm[N];
vector<int> topo;

void dfs(int v) {
  if (vis[v]) return ;
  vis[v] = true;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    dfs(u);
  }
  topo.push_back(v);
}

void par() {
  for (int i = 0; i < topo.size(); ++i)
    cout << topo[i] << " ";
  cout << endl;
}
bool comp(const int& a, const int& b) {
  cout << a << " " << b << endl;
  par(); cout << endl;
  if (a > b && h[a].find(b) == h[a].end()) {
    return false;
  }
  return true;
}


bool comp2(int& a,int& b) {
  if (a > b) return false;
  return true;
}

int main() {
  cin >> n >> m;
  while (m--) {
    int u, v;
    cin >> u >> v;
    g[u].push_back(v);
    h[u].insert(v);
  }

  for (int i = 1; i <= n; ++i)
    dfs(i);
  cout << topo.size() << endl;

  for (int i = 0; i < n / 2; ++i) {
    swap(topo[i], topo[n - i - 1]);
  }

  par();
  sort(topo.begin(), topo.end(), comp);
  par();
  for (int i = 1; i <= n; ++i) {
    perm[topo[i - 1]] = i;  
  }

  for (int i = 1; i <= n; ++i)
    cout << perm[i] << " ";
  cout << endl;


  return 0;
}