// https://www.hackerrank.com/contests/gs-codesprint/challenges/currencies/problem

#include <bits/stdc++.h>
using namespace std;

long long n;
long long x, s, f, m;
const long long N = 20;
long long e[N][N][4];
long long res[N][N][4];
long long tmp[N][N][4];
vector<int> v({2, 3, 5, 7});
vector<double> lv;
const long long MOD = 1e9 + 7;

long long ex(long long a, long long b) {
  long long res = 1;
  while (b) {
    if (b & 1) {
      res *= a;
      res %= MOD;
    }
    a *= a;
    a %= MOD;
    b >>= 1;
  }
  return res;
}


void cp(long long tmp[][N][4], long long a[][N][4], long long b[][N][4], long long i, long long j, long long k) {
  for (long long l = 0; l < 4; ++l)
    tmp[i][j][l] = a[i][k][l] + b[k][j][l];
}

bool gt(long long tmp[][N][4], long long a[][N][4], long long b[][N][4], long long i, long long j, long long k) {
  double lhs = 0;
  for (long long l = 0; l < 4; ++l)
    lhs += lv[l] * tmp[i][j][l];
  double rhs = 0;
  for (long long l = 0; l < 4; ++l)
    rhs += lv[l] * (a[i][k][l] + b[k][j][l]);
  return rhs > lhs;
}

void mult(long long a[][N][4], long long b[][N][4], bool aok = false, bool bok = false) {
  for (long long i = 0; i < n; ++i) {
    for (long long j = 0; j < n; ++j) {
      if (aok && i == j) continue;
      for (long long k = 0; k < 4; ++k)
        tmp[i][j][k] = 0;
      for (long long k = 0; k < n; ++k) {
        if (bok && j == k) continue;
        if (gt(tmp, a, b, i, j, k))
          cp(tmp, a, b, i, j, k);
      }
    }
  }
  for (long long i = 0; i < n; ++i)
    for (long long j = 0; j < n; ++j)
      for (long long k = 0; k < 4; ++k)
        a[i][j][k] = tmp[i][j][k];
}

int main() {
  cin >> n;
  cin >> x >> s >> f >> m;
  for (long long i = 0; i < n; ++i) {
    for (long long j = 0; j < n; ++j) {
      long long ai; cin >> ai;
      for (long long k = 0; k < 4; ++k) {
        // res[i][j][k] = 1; 
        while (ai && ai % v[k] == 0) {
          e[i][j][k]++;
          ai /= v[k];
        }
      }
    }
  }

  for (long long i = 0; i < 4; ++i)
    lv.push_back(log(v[i]));

  if (m > 1) {
    mult(e, e, true, true);
    m >>= 1;
  }
  while (m) {
    if (m & 1) mult(res, e);
    mult(e, e);
    m >>= 1;
  }

  long long ans = x;
  for (long long i = 0; i < 4; ++i) {
    long long ps = res[s][f][i];
    ans *= ex(v[i], ps);
    ans %= MOD;
  }
  cout << ans << endl;

  return 0;
}