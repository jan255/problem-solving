#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
const long long MOD = 1e9 + 7;
int n;
vector<int> g[N];
vector<int> dg[N];
bool yes[N];
int root;

long long cnts[N];
int yesc[N];

long long DP[N][2];

void dfs2(int v, int p = -1) {
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u != p) {
      dg[v].push_back(u);
      dfs2(u, v);
    }
  }
}

long long dp(int v, int in) {
  if (dg[v].size() == 0) return in;
  if (DP[v][in] != -1) return DP[v][in];
  long long sol = 0;
  if (in) {
    sol = 1;
    for (int i = 0; i < dg[v].size(); ++i) {
      int u = dg[v][i];
      sol = (sol * (dp(u, 1) + 1)) % MOD;
    }
  } else {
    sol = 0;
    for (int i = 0; i < dg[v].size(); ++i) {
      int u = dg[v][i];
      sol += dp(u, 0) + dp(u, 1);
      sol %= MOD;
    }
  }
  return DP[v][in] = sol;
}

void dfs(int v, int p = -1) {
  cnts[v] = 1;
  long long pi = 1;
  yesc[v] = yes[v];
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u == p) continue;

    dfs(u, v);
    pi = (pi * cnts[u]) % MOD;

    yesc[v] += yesc[u];
  }
  cnts[v] += pi;
}

long long pi = 1;

void mult(int v, int p = -1) {
  if (yesc[v] == 0) {
    pi = (pi * cnts[v]) % MOD;
    return;
  }
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u != p) {
      mult(u, v);
    }
  }
}

int main() {
  int t; cin >> t;
  while (t--) {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
      cnts[i] = -1;
      yesc[i] = 0;
      yes[i] = 0;
      g[i].clear();
    }
    pi = 1;

    root = 1;
    int k = 0;
    for (int i = 1; i < n; ++i) {
      int u, v; cin >> u >> v;
      g[u].push_back(v);
      g[v].push_back(u);
      int y; cin >> y;
      if (y) {
        k++;
        yes[u] = yes[v] = 1;
        root = u;
      }
    }

    if (k) {
      dfs(root);
      mult(root);
      cout << pi << endl;
    } else {
      for (int i = 1; i <= n; ++i) {
        DP[i][0] = DP[i][1] = -1;
        dg[i].clear();
      }
      dfs2(root);
      cout << (dp(root, 0) + dp(root, 1)) % MOD << endl; 
    }

  } 

  return 0;
}