#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
vector<pair<int, int> > one;
vector<pair<int, int> > two;
int p[N];
int t[N];

int main() {
  int n, q;
  cin >> n >> q;
  for (int i = 0; i < n; ++i) {
    cin >> t[i];
  }
  for (int i = 0; i < n; ++i) {
    cin >> p[i];
    one.push_back(make_pair(p[i], t[i]));
    two.push_back(make_pair(t[i], p[i]));
  }

  vector<int> o;
  sort(one.begin(), one.end());
  for (int i = 0; i < n; ++i)
    o.push_back(one[i].second);
  for (int i = n - 2; i >= 0; --i)
    o[i] = min(o[i], o[i + 1]);

  vector<int> tw;
  sort(two.begin(), two.end());
  for (int i = 0; i < n; ++i)
    tw.push_back(two[i].second);
  for (int i = n - 2; i >= 0; --i)
    tw[i] = max(tw[i], tw[i + 1]);

  while (q--) {
    int type, v;
    cin >> type >> v;
    if (type == 1) {
      if (one[n - 1].first < v) {
        cout << -1;
      } else {
        int l = 0, r = n - 1;
        while (l < r) {
          int mid = (l + r) >> 1;
          if (one[mid].first >= v) r = mid;
          else l = mid + 1;
        }
        cout << o[l];
      }
    } else if (type == 2) {
      if (v > two[n - 1].first) {
        cout << -1;
      } else {
        int l = 0, r = n - 1;
        while (l < r) {
          int mid = (l + r) >> 1;
          if (two[mid].first >= v) r = mid;
          else l = mid + 1;
        }
        cout << tw[l];
      }
    }
    cout << endl;
  }

  return 0;
}