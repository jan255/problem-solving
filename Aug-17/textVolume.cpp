// http://codeforces.com/contest/837/problem/A
#include <bits/stdc++.h>
using namespace std;


int main() {
  int n; cin >> n;
  string s; getline(cin, s);
  getline(cin, s);
  int c = 0; int vol = 0;
  for (int i = 0; i < n; ++i) {
    c += s[i] >= 'A' && s[i] <= 'Z'; 
    if (s[i] == ' ') {
      vol = max(vol, c);
      c = 0;
    }
  }
  cout << max(c, vol) << endl;

  return 0;
}