#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 2;
int n, m, k;
long long at[N], a[N];
long long ops[N];
vector<int> v;

int main() {
  cin >> n >> m >> k;
  for (int i = 1; i <= n; ++i)
    cin >> at[i];
  for (int i = 0; i < m; ++i) {
    int l, r, d;
    cin >> l >> r >> d;
    v.push_back(l);
    v.push_back(r);
    v.push_back(d);
  }
  while (k--) {
    int x, y;
    cin >> x >> y;
    ops[x]++;
    ops[y + 1]--;
  }
  for (int i = 2; i <= m; ++i)
    ops[i] += ops[i - 1];
  for (int i = 0; i < 3 * m; i += 3) {
    int l = v[i];
    int r = v[i + 1];
    int d = v[i + 2];
    a[l] += ops[i / 3 + 1] * 1LL * d;
    a[r + 1] -= ops[i / 3 + 1] * 1LL * d;
  }
  for (int i = 1; i <= n; ++i) {
    a[i] += a[i - 1];
    cout << at[i] + a[i] << " ";
  }
  cout << endl;

  return 0;
}