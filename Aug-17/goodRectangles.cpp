#include <bits/stdc++.h>
using namespace std;

const int N = 40;
int n, m, q;
string side[N];
int DP[N][N][N][N];
int RDP[N][N][N][N];

int isRect(int a, int b, int x, int y) {
  if (a > x || b > y) return 1;
  if (RDP[a][b][x][y] != -1)
    return RDP[a][b][x][y];
  return RDP[a][b][x][y] = 
          side[x][y] == '0' and isRect(a, b, x - 1, y) and isRect(a, b, x, y - 1);
}

int numRects(int a, int b, int x, int y) {
  int cnt = 0;
  for (int i = a; i <= x; ++i)
    for (int j = b; j <= y; ++j)
      cnt += isRect(i, j, x, y);
  return cnt;
}

int dp(int a, int b, int x, int y) {
  if (a > x || b > y) return 0;
  if (DP[a][b][x][y] != -1) return DP[a][b][x][y];
  int sol = dp(a, b, x - 1, y) + dp(a, b, x, y - 1) - dp(a, b, x - 1, y - 1);
  sol += numRects(a, b, x, y);
  return DP[a][b][x][y] = sol;
}

int main() {
  cin >> n >> m >> q;
  for (int i = 0; i < n; ++i)
    cin >> side[i];

  for (int i = 0; i < N; ++i)
    for (int j = 0; j < N; ++j)
      for (int a = 0; a < N; ++a)
        for (int b = 0; b < N; ++b)
          DP[i][j][a][b] = RDP[i][j][a][b] = -1;

  while (q--) {
    int a, b; cin >> a >> b;
    int x, y; cin >> x >> y;
    a--; b--; x--; y--;
    cout << dp(a, b, x, y) << endl;
  }

  return 0;
}