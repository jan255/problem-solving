#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int n;
vector<int> g[N];
set<int> st;
int bads[N];
vector<int> ans;

void dfs1(int v, int p = -1) {
  if (st.find(v) != st.end()) bads[v] = 1;
  for (int i = 0; i < g[v].size(); ++i) {
    if (g[v][i] != p) {
      dfs1(g[v][i], v);
      bads[v] += bads[g[v][i]];
    }
  }
  if (bads[v] == 1) ans.push_back(v);
}

int main() {
  cin >> n;
  for (int i = 0; i < n - 1; ++i) {
    int u, v; cin >> u >> v;
    g[u].push_back(v);
    g[v].push_back(u);
    int bad; cin >> bad;
    if (bad == 2) {
      st.insert(u);
      st.insert(v);
    }
  }
  dfs1(1);
  cout << ans.size() << endl;
  for (int i = 0; i < ans.size(); ++i)
    cout << ans[i] << " ";
  cout << endl;


  return 0;
}