#include <bits/stdc++.h>
using namespace std;


int main() {
  int r, g, b;
  cin >> r >> g >> b;
  int bs = min(r, min(g, b));
  r -= bs;
  g -= bs;
  b -= bs;

  vector<int> v;
  v.push_back(r);
  v.push_back(g);
  v.push_back(b);
  sort(v.begin(), v.end());

  if (v[1]) {
    int add = min(v[1], v[2] / 2);
    v[1] -= add;
    v[2] -= 2 * add;
    if (v[2] && v[1] >= 2) add++;
    bs += add;
  }
  cout << bs << endl;

  return 0;
}