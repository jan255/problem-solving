#include <bits/stdc++.h>
using namespace std;

struct Node {
  int val;
  Node* next;
};

Node* ll = NULL;

Node* makeNewNode(int val) {
  Node* node = (Node*)malloc(sizeof(Node));
  node->val = val;
  node->next = NULL;
  return node;
}

void add(int val) {
  Node* newNode = makeNewNode(val);
  newNode->next = ll;
  ll = newNode;
}

void print() {
  Node* tmp = ll;
  while (tmp != NULL) {
    cout << (tmp->val) << " ";
    tmp = tmp->next;
  }
  cout << endl;
}

void remove(Node* node) {
  Node* next = node->next;
  node->val = next->val;
  node->next = next->next;

  next->next = NULL;
  free(next);
}

Node* reverse(Node* head) {
  if (head == NULL) return NULL;

  Node* rev = reverse(head->next);
  if (head->next != NULL) {
    head->next->next = head;
    head->next = NULL;
  }
  return rev == NULL ? head : rev;
}

Node* findmid() {
  Node* behind = ll;
  Node* infront = ll->next;
  while (infront != NULL and infront->next != NULL) {
    behind = behind->next;
    infront = infront->next;
    infront = infront->next;
  }
  cout << "mid " << (behind != NULL ? (behind->val) : -1)  << endl;
  return behind;
}

bool isPalin(Node* l, Node* r) {
  while (r != NULL) {
    if (l->val != r->val) return false;
    l = l->next;
    r = r->next;
  }
  return true;
}

int x = 1;

int main() {

  int x = 2;

  cout << ::x << " " << x << endl;    
  
  // for (int i = 0; i < 14; ++i) {
  //   int x = random() % 8;
  //   add(x);
  // }
  // print();

  // Node* mid = findmid();
  // mid->next = reverse(mid->next);
  // print();

  // cout << (isPalin(ll, mid->next) ? "palin " : "not palin ") << endl;;

 
  return 0;
}