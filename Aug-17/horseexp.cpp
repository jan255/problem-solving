#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 5;
vector<int> g[N];
int n;
double p[N];
double ans = 0;

void dfs(int v, int par = -1, double l = 0) {
  int outs = g[v].size() - (par != -1);
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u != par) {
      p[u] = p[v] / outs;
      dfs(u, v, l + 1);
    }
  }
  if (par != -1 and g[v].size() == 1) {
    ans += l * p[v];
  }
}

int main() {
  cin >> n;
  for (int i = 1; i < n; ++i) {
    int u, v; cin >> u >> v;
    g[u].push_back(v);
    g[v].push_back(u);
  }

  p[1] = 1;
  dfs(1);
  cout << fixed << setprecision(7) << ans << endl;

  return 0;
}