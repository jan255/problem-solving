#include <bits/stdc++.h>
using namespace std;

const int N = 2e5 + 1;
long long ps[N];

int main() {
  ios::sync_with_stdio(false);
  
  int n; cin >> n;
  int q; cin >> q;
  vector<long long> v;
  while (n--) {
    int x; cin >> x;
    v.push_back(x);
  }
  sort(v.begin(), v.end());
  reverse(v.begin(), v.end());
  while (q--) {
    int l, r; cin >> l >> r;
    ps[l]++; ps[r + 1]--;
  }
  vector<long long> times;
  for (int i = 1; i < N; ++i) {
    ps[i] += ps[i - 1];
    if (ps[i]) times.push_back(ps[i]);
  }
  sort(times.begin(), times.end());
  reverse(times.begin(), times.end());
  long long sum = 0;
  for (int i = 0; i < times.size(); ++i) {
    sum += times[i] * v[i];
  }
  cout << sum << endl;


  return 0;
}