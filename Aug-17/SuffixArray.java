import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Suffix Array implementation in NlogN time
 * Suffix Array -> Sorted indexes of suffixes for a string
 * 
 * @author abhilash
 */
public class SuffixArray {

	class Rank implements Comparable<Rank> {
		int firstHalf;
		int secondHalf;
		int idx;
		
		public int compareTo(Rank o) {
			if (this.firstHalf == o.firstHalf) {
				return this.secondHalf - o.secondHalf;
			}
			return this.firstHalf - o.firstHalf;
		}
	}
	
	private Rank[] sorted;
	private String str;
	
	private int[][] ranks;
	
	public SuffixArray(String s) {
		this.str = s.substring(0); // clone
		this.sorted = new Rank[s.length()];
		
		this.ranks = new int[s.length()][(int) (Math.log(s.length()) + 3)];
		
		for (int i = 0; i < s.length(); ++i) {
			this.sorted[i] = new Rank();
			
			this.ranks[i][0] = s.charAt(i) - 'a';
		}
		this.sort();
	}
	
	private void sort() {
		for (int i = 1; (1 << i) < this.str.length(); ++i) {
		
			for (int j = 0; j < this.str.length(); ++j) {
				this.sorted[j].idx = j;
				this.sorted[j].firstHalf = this.ranks[j][i - 1];
			}
		
			int plus = 1 << (i - 1);
			for (int j = 0; j < this.sorted.length; ++j) {
				this.sorted[j].secondHalf = j + plus < this.str.length() ? 
						this.sorted[j + plus].firstHalf : -1;
			}
			Arrays.sort(this.sorted);

			int rank = 0;
			this.ranks[this.sorted[0].idx][i] = 0;
			for (int j = 1; j < this.sorted.length; ++j) {
				if  (this.sorted[j].firstHalf != this.sorted[j - 1].firstHalf || 
						this.sorted[j].secondHalf != this.sorted[j - 1].secondHalf) {
					rank++;
				}
				this.ranks[this.sorted[j].idx][i] = rank;
			}
		}
	}
	
	public void printSortedSuffixes() {
		for (Rank idx : this.sorted) {
			System.out.println(this.str.substring(idx.idx));
		}
	}
	
	public static void main(String... args) {
		SuffixArray arr = new SuffixArray("adfiauhrnkxcjhviuerthownfxncjbahlsfdhae");
		arr.printSortedSuffixes();
	}

}
