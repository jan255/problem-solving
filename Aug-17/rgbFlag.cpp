#include <bits/stdc++.h>
using namespace std;

string f[123];

bool isFlag(string s[], int n, int m) {
  int r = 0;
  int arr[3];
  arr[0] = arr[1] = arr[2] = 0;
  for (int k = 0; k < 3; ++k) {
    int c = s[k * (n / 3)][0];
    for (int i = k * (n / 3); i < (k + 1) * (n / 3); ++i) {
      for (int j = 0; j < m; ++j) {
        arr[r] += (c == s[i][j]);
      } 
    }
    r++;
  }
  return arr[0] == arr[1] && arr[1] == arr[2];
}

int main() {
  int n, m;
  cin >> n >> m;
  for (int i = 0; i < n; ++i)
    cin >> f[i]; 
  bool flag = false;
  if (n % 3 == 0) {
    flag = isFlag(f, n, m);
  }
  if (m % 3 == 0) {
    string s[123];
    for (int i = 0; i < m; ++i)
      s[i].resize(n);
    for (int i = 0; i < n; ++i) {
      for (int j = 0; j < m; ++j) {
        s[j][i] = f[i][j];
      }
    }
    flag = flag || isFlag(s, m, n);
  }
  cout << (flag ? "YES" : "NO") << endl;

  return 0;
}