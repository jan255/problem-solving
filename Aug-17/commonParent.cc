
vector<int> g[N];
int h[N];
int par[N];

void fillHeights(int v, int p = -1) {
  h[v] = 0;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u == p) continue;

    par[u] = v;
    fillHeights(u, v);
    h[v] = max(h[v], h[u]);
  }
  h[v]++;
}

void findCommonPar(int u, int v) {
  if (h[u] > h[v]) swap(u, v);
  while (h[v] > h[u]) v = par[v];

  while (u != v) {
    u = par[u];
    v = par[v];
  }
  return v;
}

node makeMinBst(int[] arr, int l, int r) {
  int mid = (l + r) >> 1;
  node = makeNode(arr[mid]);
  node.left = makeMinBst(arr, l, mid - 1);
  node.right = makeMinBst(arr, mid + 1, r);
  return node;
}

void minBst(int[] arr, int l) {
  bst = makeMinBst(arr, 0, l - 1);  
}


bool isConnected(int u, int v) {
  queue<int> q;
  bool vis[N] = {false};

  q.push(u);
  while (!q.empty()) {
    int ver = q.front(); q.pop();
    if (ver == v) return true;
    for (int i = 0; i < g[ver].size(); ++i) {
      int neigh = g[ver][i];
      if (!vis[neigh]) {
        q.push(neigh);
        vis[neigh] = true;
      }
    }
  }

  return false;
}


int idx[N];
int par[N];

void dfs(int v, int c = 0, int p = -1) {
  idx[v] = c++;
  for (int i = 0; i < g[v].size(); ++i) {
    int u = g[v][i];
    if (u == p) continue;

    par[idx[v]] = p != -1 ? idx[p] : -1;
    dfs(u, c, v);
  }
}

int sub[N];

