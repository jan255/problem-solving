// https://www.hackerrank.com/contests/openbracket-2017/challenges/sliding-inversion
#include <bits/stdc++.h>
using namespace std;

int n, m;
const int N = 5e5 + 10;
int ar[N];
int bit[N];

void add(int x) {
  while (x < N) {
    bit[x]++;
    x += x & -x;
  }
}

void rm(int x) {
  while (x < N) {
    bit[x]--;
    x += x & -x;
  }
}

int sum(int x) {
  int cnt = 0;
  while (x > 0) {
    cnt += bit[x];
    x -= x & -x;
  }
  return cnt;
}

int lt(int x) {
  return sum(x - 1);
}

int gt(int x) {
  return sum(N - 1) - sum(x);
}

int main() {
  cin >> n >> m;
  vector<int> v;
  for (int i = 0; i < n; ++i) {
    cin >> ar[i];
    v.push_back(ar[i]);
  }
  sort(v.begin(), v.end());
  map<int, int> mp;
  for (int i = 0, j = 1; i < v.size(); ++i) {
    if (!mp[v[i]]) mp[v[i]] = j++;
  }
  for (int i = 0; i < n; ++i) {
    ar[i] = mp[ar[i]];
    if (i < m) add(ar[i]);
  }

  long long ic = 0;
  for (int i = 0; i < m; ++i) {
    ic += lt(ar[i]);
    rm(ar[i]);
  }
  long long sic = ic;
  for (int i = 0; i < m; ++i) add(ar[i]);
  for (int i = m; i < n; ++i) {
    ic -= lt(ar[i - m]);
    rm(ar[i - m]);
    ic += gt(ar[i]);
    add(ar[i]);
    sic += ic;
  }

  cout << sic << endl;
  return 0;
}