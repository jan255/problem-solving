#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
int id[N];
int n;
int st[4 * N];

int build(int l, int r, int i = 0) {
  if (l > r) return -1;
  if (l == r) {
    st[i] = l;
    return l;
  }
  int mid = (l + r) >> 1;
  int li = build(l, mid, 2 * i + 1);
  int ri = build(mid + 1, r, 2 * i + 2);
  if (li == -1) st[i] = ri;
  else if (ri == -1) st[i] = li;
  else {
    st[i] = id[li] > id[ri] ? li : ri;
  }
  return st[i];
}

int find(int l, int r, int x = 0, int y = n - 1, int i = 0) {
  if (r < x || l > y) return -1;
  if (l <= x && r >= y) return st[i];
  int mid = (x + y) >> 1;
  int li = find(l, r, x, mid, 2 * i + 1);
  int ri = find(l, r, mid + 1, y, 2 * i + 2);
  if (li == -1) return ri;
  if (ri == -1) return li;
  return (id[li] > id[ri]) ? li : ri;
}

// rem, steps
pair<int, int> solve(int l, int r) {
  if (l > r) return make_pair(0, 0);
  if (l == r) return make_pair(1, 0); 
  int maxi = find(l, r);
  pair<int, int> back = solve(l, maxi - 1);
  pair<int, int> forw = solve(maxi + 1, r);
  int addSteps = max(back.second, forw.first + forw.second);
  int rem = back.first + 1;
  return make_pair(rem, addSteps);
}

int main() {
  cin >> n; 
  for (int i = 0; i < n; ++i) {
    cin >> id[i];
  }
  build(0, n - 1);
  cout << solve(0, n - 1).second << endl;

  return 0;
}