#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 15;
int A[N];
int B[N];

int main() {
  int T; cin >> T;
  while (T--) {
    int n, k; cin >> n >> k;
    for (int i = 0; i < n; ++i) {
      cin >> A[i];
    }
    int mi = 0;
    long long ms = 0;
    for (int i = 0; i < n; ++i) {
      cin >> B[i];
      if (abs(B[mi]) < abs(B[i])) mi = i;
      ms += A[i] * 1LL * B[i];
    }

    ms += abs(B[mi]) * 1LL * k;

    cout << ms << endl;
  } 

  return 0;
}