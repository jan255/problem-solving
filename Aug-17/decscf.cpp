#include <bits/stdc++.h>
using namespace std;


int main() {
  int n, k;
  cin >> n >> k;
  int c = 0;
  int left = 0;
  for (int i = 0; i < n; ++i) {
    int a; cin >> a;
    left += a;
    c += min(8, left);
    left -= min(8, left);

    if (c >= k) {
      cout << i + 1;
      return 0;
    }
  }
  cout << -1;

  return 0;
}