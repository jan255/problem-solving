#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 1;
long long n, m, k;
vector<int> v;
long long ps[N];
long long is[N];

int main() {
  cin >> n >> m >> k; 
  for (int i = 0; i < n; ++i) {
    int b; cin >> b;
    if (b) v.push_back(i);
  }

  if (v.size() <= m) {
    cout << -1 << endl;
    exit(0);
  }

  ps[0] = v[0];
  is[0] = 0;
  for (int i = 1; i < v.size(); ++i) {
    ps[i] = ps[i - 1] + v[i] - v[i - 1];
    is[i] = is[i - 1] + (v[i] - v[i - 1]) * i * k;
  }

  long long min = -1;
  for (int i = 0; i + m <= v.size(); ++i) {
    long long sol = is[i + m - 1] - (i > 0 ? is[i - 1] : 0);
    sol -= i * k * (ps[i + m - 1] - (i > 0 ? ps[i - 1] : 0));
    if (min == -1 || min < sol + v[i]) min = sol + v[i];
  }
  cout << min << endl;


  return 0;
}