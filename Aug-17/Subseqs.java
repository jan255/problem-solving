import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Subseqs {

    class Pair implements Comparable<Pair> {
      int i, j;
      
      Pair(int i, int j) {
        this.i = i;
        this.j = j;
      }

      public int compareTo(Pair p) {
        if (this.i != p.i) return this.i - p.i;
        return this.j - p.j;
      }
      
      public boolean equals(Pair p) {
        return this.i == p.i && this.j == p.j;
      }
      
      public int hashCode() {
        return (this.i << 14) | this.j;
      }
    }
  
    private Map<Pair, Integer> DP = new TreeMap<>();
  
    int dp(String a, String b, int ai, int bi) {
      if (ai < 0 || bi < 0) return 0;
      if (DP.containsKey(new Pair(ai, bi))) 
        return DP.get(new Pair(ai, bi));
      int sol = 0;
      if (a.charAt(ai) == b.charAt(bi))
        sol = dp(a, b, ai - 1, bi - 1) + 1;
      sol = Math.max(sol,
                     Math.max(dp(a, b, ai - 1, bi), dp(a, b, ai, bi - 1)));
      DP.put(new Pair(ai, bi), sol);
      return sol;
    }
  
    static int commonChild(String s1, String s2){
      return new Subseqs().dp(s1, s2, s1.length() - 1, s2.length() - 1);
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String s1 = in.next();
        String s2 = in.next();
        int result = commonChild(s1, s2);
        System.out.println(result);
    }
}
