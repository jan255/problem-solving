/**
 * Definition for binary tree
 * struct TreeNode {
 *     int val;
 *     TreeNode *left;
 *     TreeNode *right;
 *     TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 * };
 */
/**
 * Definition for singly-linked list.
 * struct ListNode {
 *     int val;
 *     ListNode *next;
 *     ListNode(int x) : val(x), next(NULL) {}
 * };
 */
#include <bits/stdc++.h>
using namespace std;


 struct ListNode {
      int val;
      ListNode *next;
      ListNode(int x) : val(x), next(NULL) {}
  };

struct TreeNode {
      int val;
      TreeNode *left;
      TreeNode *right;
      TreeNode(int x) : val(x), left(NULL), right(NULL) {}
 };

int findLeftSize(ListNode* node) {
  int len = 0;
  for (ListNode* tmpPtr = node; tmpPtr != NULL; tmpPtr = tmpPtr->next) {
    ++len;
  }
  
  --len;
  int levelNodes = 1;
  int leftSize = 0;
  while ((levelNodes << 1) <= len) {
    cout << levelNodes << endl;
    len -= (levelNodes << 1);
    leftSize += (levelNodes);
    levelNodes <<= 1;
  }

  leftSize += min(len, levelNodes);
  return leftSize;
}


// TreeNode* Solution::sortedListToBST(ListNode* A) {
TreeNode* sortedListToBST(ListNode* A) {
  if (A == NULL) return NULL;
  if (A->next == NULL) return new TreeNode(A->val);

  int sizeLeftTree = findLeftSize(A);
  cout << sizeLeftTree << endl;
  ListNode* leftList = A;
  ListNode* root = A;
  for (int i = 0; i < sizeLeftTree - 1; ++i) {
    root = root->next;
  }
  ListNode* leftTail = root;
  root = root->next;
  leftTail->next = NULL;

  TreeNode* leftBst = sortedListToBST(A);
  TreeNode* rightBst = sortedListToBST(root->next);
  TreeNode* currRoot = new TreeNode(root->val);
  currRoot->left = leftBst;
  currRoot->right = rightBst;
  
  return currRoot;
}


int main() {
  ListNode* tail = new ListNode(2);
  ListNode* head = new ListNode(1);
  head->next = tail;

  sortedListToBST(head);


  return 0;
}